package me.FurH.CreativeControl.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import me.FurH.Bukkit.Core.util.BCommunicator;
import me.FurH.Core.close.Closer;
import me.FurH.Core.database.SQLDb;
import me.FurH.Core.encript.Base64;
import me.FurH.Core.file.FileUtils;
import me.FurH.Core.time.TimeUtils;
import me.FurH.Core.util.Utils;
import me.FurH.CreativeControl.CreativeControl;
import me.FurH.CreativeControl.EntryPoint;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 *
 * @author FurmigaHumana
 * All Rights Reserved unless otherwise explicitly stated.
 */
public class Communicator extends BCommunicator {

    @Override
    public void error(Throwable ex, String message, Object...objects) {

        EntryPoint plugin = EntryPoint.getPlugin();

        if (message != null) {
            log("§c" + message, objects);
        }

        log("§fThis error is avaliable at§8:§c plugins/{0}/error/error-{1}.txt", plugin.getDescription().getName(), stack(plugin, ex));
    }

    private String stack(EntryPoint plugin, Throwable ex) {

        String format1 = TimeUtils.getSimpleFormatedTimeWithMillis(System.currentTimeMillis());
        File data = new File(plugin.getDataFolder() + File.separator + "error");

        if (!data.exists()) {
            data.mkdirs();
        }

        try {
            FileUtils.deleteOldestFileAt(data, 10);
        } catch (IOException ex1) {
            ex1.printStackTrace();
        }

        data = new File(data.getAbsolutePath(), "error-"+format1+".txt");
        
        if (!data.exists()) {
            try {
                data.createNewFile();
            } catch (IOException e) {
                log("§fFailed to create new log file§8:§c " + e.getMessage());
            }
        }

        StringBuilder sb = new StringBuilder();
   
        try {

            String l = System.getProperty("line.separator");
            String format2 = TimeUtils.getFormatedTime(System.currentTimeMillis());

            Runtime runtime = Runtime.getRuntime();
            File root = new File("/");

            int creative = 0;
            int survival = 0;
            int totalp = Bukkit.getOnlinePlayers().length;

            for (Player p : Bukkit.getOnlinePlayers()) {
                if (p.getGameMode().equals(GameMode.CREATIVE)) {
                    creative++;
                } else {
                    survival++;
                }
            }
            
            CreativeControl cc = EntryPoint.get();
            
            sb.append(format2).append(l);
            
            sb.append("	=============================[ ERROR INFORMATION ]=============================").append(l);
            sb.append("	- Plugin: ").append(plugin.getDescription().getFullName()).append(l);
            sb.append("	- Players: ").append(totalp).append(" (").append(creative).append(" Creative, ").append(survival).append(" Survival)").append(l);
            
            sb.append("	=============================[ HARDWARE SETTINGS ]=============================").append(l);
            sb.append("		Java: ").append(System.getProperty("java.vendor")).append(" ").append(System.getProperty("java.version")).append(" ").append(System.getProperty("java.vendor.url")).append(l);
            sb.append("		System: ").append(System.getProperty("os.name")).append(" ").append(System.getProperty("os.version")).append(" ").append(System.getProperty("os.arch")).append(l);
            sb.append("		Processors: ").append(runtime.availableProcessors()).append(l);
            sb.append("		Memory: ").append(l);
            sb.append("			Free: ").append(Utils.getFormatedBytes(runtime.freeMemory())).append(l);
            sb.append("			Total: ").append(Utils.getFormatedBytes(runtime.totalMemory())).append(l);
            sb.append("			Max: ").append(Utils.getFormatedBytes(runtime.maxMemory())).append(l);
            sb.append("		Storage: ").append(l);
            sb.append("			Total: ").append(Utils.getFormatedBytes(root.getTotalSpace())).append(l);
            sb.append("			Free: ").append(Utils.getFormatedBytes(root.getFreeSpace())).append(l);
            
            SQLDb db = cc.getDb();
            if (db != null) {
                db.throwable(sb, l);
            }
                        
            sb.append("	=============================[ INSTALLED PLUGINS ]=============================").append(l);
            
            sb.append("	Plugins:").append(l);

            for (Plugin x : Bukkit.getServer().getPluginManager().getPlugins()) {
                sb.append("		- ").append(x.getDescription().getFullName()).append(l);
            }
            
            sb.append("	=============================[  LOADED   WORLDS  ]=============================").append(l);
            
            sb.append("	Worlds:").append(l);
            
            for (World w : Bukkit.getServer().getWorlds()) {
                sb.append("		").append(w.getName()).append(":").append(l);
                sb.append("			Envioronment: ").append(w.getEnvironment().toString()).append(l);
                sb.append("			Player Count: ").append(w.getPlayers().size()).append(l);
                sb.append("			Entity Count: ").append(w.getEntities().size()).append(l);
                sb.append("			Loaded Chunks: ").append(w.getLoadedChunks().length).append(l);
            }
            
            sb.append("	=============================[ START  STACKTRACE ]=============================").append(l);
            
            sb.append(getThrowableString(ex));

            sb.append("	=============================[ END OF STACKTRACE ]=============================").append(l);
            sb.append(format2);

        } catch (Exception ex1) {
            ex1.printStackTrace(); // StackOverFlow
        }

        FileOutputStream fos = null;

        try {

            fos = new FileOutputStream(data);

            fos.write("## THIS FILE IS ENCODED TO PRESERVE ITS CONTENTS. IT CAN EASILY BE DECODED USING ANY BASE64 DECODER AVAILABLE.\n".getBytes("UTF-8"));
            fos.write(Base64.encodeToByte(sb.toString().getBytes("UTF-8"), true));

            fos.flush();
            
        } catch (IOException ex1) {
            ex1.printStackTrace();
        } finally {
            Closer.closeQuietly(fos);
        }
        
        return format1;
    }

    public String getThrowableString(Throwable ex) {

        StringWriter swriter = new StringWriter();
        PrintWriter pwriter = new PrintWriter(swriter);
        
        ex.printStackTrace(pwriter);

        return swriter.toString();
    }
}