package me.FurH.CreativeControl.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import me.FurH.Core.close.Closer;
import me.FurH.Core.database.SQLDb;
import me.FurH.Core.database.SQLDb.CoreType;
import me.FurH.Core.debug.Log;
import me.FurH.CreativeControl.CreativeControl;
import me.FurH.CreativeControl.config.MainNodePack;
import me.FurH.CreativeControl.messages.Messages;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.World;

/**
 *
 * @author FurmigaHumana
 * All Rights Reserved unless otherwise explicitly stated.
 */
public class DatabaseManager {

    public void initialize(SQLDb database, MainNodePack config) {

        Log.log(Messages.prefix + "Loading database...");

        for (World world : Bukkit.getWorlds()) {
            loadWorld(world.getName(), database, config);
        }

        initGroupsTable(database, config);
        initFriendsTable(database, config);
        initPlayersTable(database, config);
        initRegionsTable(database, config);
        initGameModeTable(database, config);
        updateGameModeTable(database, config);
        
        try {
            database.commit(true);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        try {
            database.setAutoCommit(false);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
        Log.log(Messages.prefix + "Database Ready");
    }
    
    private void updateGameModeTable(SQLDb database, MainNodePack config) {
        
        for (GameMode gm : GameMode.values()) {

            String table = config.database_prefix + "players_" + gm.toString().toLowerCase();
            
            if (database.hasTable(table)) {

                Log.log("    -  table§8:§a " + table + "§f...");
                
                Statement st = null;
                ResultSet rs = null;
                boolean delete = true;

                PreparedStatement ps = null;
                
                try {

                    st = database.statement(true);
                    st.execute("SELECT player, armor, inventory FROM `" + table + "`;");

                    rs = st.getResultSet();

                    ps = database.prepare("INSERT INTO `" + config.database_prefix + "inventory_" + gm.toString().toLowerCase() + "` (player, status, armor, contents) VALUES (?, ?, ?, ?);", true);
                    int batch = 0;
                    
                    while (rs.next()) {
                        
                        ps.setInt(1, rs.getInt(1));
                        ps.setString(2, "[]");
                        ps.setString(3, rs.getString(2));
                        ps.setString(4, rs.getString(3));

                        batch++;
                        
                        if (batch > 10) {
                            
                            ps.execute();
                            batch = 0;
                            
                        } else {
                            ps.addBatch();
                        }
                    }
                    
                    if (batch > 0) {
                        ps.execute();
                    }

                } catch (Throwable ex) {

                    ex.printStackTrace();
                    delete = false;

                } finally {
                    Closer.closeQuietly(st, rs, ps);
                }
                
                if (delete) {
                    
                    try {
                        
                        Log.log("    - Drop table§8:§a " + table + "§f...");
                        
                        st = database.statement(true);
                        st.execute("DROP TABLE `" + table + "`;");
                        
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    } finally {
                        Closer.closeQuietly(st);
                    }
                }
            }
        }
    }
    
    private void initGameModeTable(SQLDb database, MainNodePack config) {
        
        for (GameMode gm : GameMode.values()) {

            String table = config.database_prefix + "inventory_" + gm.toString().toLowerCase();

            if (!database.hasTable(table)) {

                String sql =

                        "CREATE TABLE IF NOT EXISTS `"+ table +"` ("
                            + "`player` int(10) unsigned NOT NULL,"
                            + "`status` varchar(255) NOT NULL,"
                            + "`armor` text NOT NULL,"
                            + "`contents` text NOT NULL"
                        + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";

                Log.log("    - Creating table§8:§a " + table + "§f...");

                try {
                    database.createTable(sql);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }

            try {

                String indexname = gm.toString().toLowerCase() + "_search_index";
                if (!database.hasIndex(table, indexname)) {

                    String unique = "CREATE UNIQUE INDEX `" + indexname + "` ON `" + table + "` (`player`);";
                    Log.log("    - Creating index §8:§a " + indexname + "§f...");

                    try {
                        database.createIndex(unique);
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                }
                
            } catch (SQLException ex) {
                Log.error(ex, "Failed to check if table has index.");
            }
        }
    }
    
    private void initRegionsTable(SQLDb database, MainNodePack config) {
        
        String table = config.database_prefix + "regions";
        
        if (!database.hasTable(table)) {
            
            String sql =
                    
                    "CREATE TABLE IF NOT EXISTS `"+ table +"` ("
                        + "{auto},"
                        + "`name` varchar(255) NOT NULL,"
                        + "`start` varchar(255) NOT NULL,"
                        + "`end` varchar(255) NOT NULL,"
                        + "`type` varchar(255) NOT NULL"
                    + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            Log.log("    - Creating table§8:§a " + table + "§f...");

            try {
                database.createTable(sql);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    private void initPlayersTable(SQLDb database, MainNodePack config) {

        boolean uuids = CreativeControl.isUUID();
        String table = config.database_prefix + (uuids ? "uuids" : "players");

        if (!database.hasTable(table)) {
            
            String sql =
                    
                    "CREATE TABLE IF NOT EXISTS `"+ table +"` ("
                        + "{auto},"
                        + "`" + (uuids ? "uuid" : "player") + "` varchar(255) NOT NULL"
                    + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            Log.log("    - Creating table§8:§a " + table + "§f...");

            try {
                database.createTable(sql);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        
        try {
            
            String indexname = "players_" + (uuids ? "uuid" : "name") + "_search";
            if (!database.hasIndex(table, indexname)) {
                
                String index = "CREATE UNIQUE INDEX {exists} `" + indexname + "` ON `"+table+"` (`player`);";
                Log.log("    - Creating index §8:§a " + indexname + "§f...");
                
                try {
                    database.createIndex(index);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
            
        } catch (SQLException ex) {
            Log.error(ex, "Failed to check if table has index.");
        }
    }
    
    private void initGroupsTable(SQLDb database, MainNodePack config) {
        
        String table = config.database_prefix + "groups";
        
        if (!database.hasTable(table)) {
            
            String sql =
                    
                    "CREATE TABLE IF NOT EXISTS `"+ table +"` ("
                        + "`player` int(11) unsigned NOT NULL,"
                        + "`groups` varchar(255) NOT NULL"
                    + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            Log.log("    - Creating table§8:§a " + table + "§f...");

            try {
                database.createTable(sql);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        
        try {

            String indexname = "groups_index_search";
            if (!database.hasIndex(table, indexname)) {
                
                String unique = "CREATE UNIQUE INDEX `" + indexname + "` ON `" + table + "` (`player`);";
                Log.log("    - Creating index §8:§a " + indexname + "§f...");
                
                try {
                    database.createIndex(unique);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }

        } catch (SQLException ex) {
            Log.error(ex, "Failed to check if table has index.");
        }
    }

    private void initFriendsTable(SQLDb database, MainNodePack config) {
        
        String table = config.database_prefix + "friends";
        
        if (!database.hasTable(table)) {
            
            String sql = 

                    "CREATE TABLE IF NOT EXISTS `"+ table +"` ("
                        + "`player` int(11) unsigned NOT NULL,"
                        + "`friends` varchar(255) NOT NULL"
                    + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            Log.log("    - Creating table§8:§a " + table + "§f...");

            try {
                database.createTable(sql);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

        if (database.hasField(table, "id")) {
            
            String drop = "DROP TABLE `" + table + "`;";

            Log.log("    - Truncate table§8:§a " + table + "§f...");
            
            try {
                database.statement(true).execute(drop);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        
        try {

            String indexname = "friends_index_search";
            if (!database.hasIndex(table, indexname)) {

                String unique = "CREATE UNIQUE INDEX `" + indexname + "` ON `" + table + "` (`player`);";
                Log.log("    - Creating index §8:§a " + indexname + "§f...");

                try {
                    database.createIndex(unique);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }

        } catch (SQLException ex) {
            Log.error(ex, "Failed to check if table has index.");
        }
    }

    public void loadWorld(String name, SQLDb database, MainNodePack config) {

        String table = config.database_prefix + "blocks_" + name;
        clean(table, name, database, config);

        if (!database.hasTable(table)) {
            
            String sql = 

                    "CREATE TABLE IF NOT EXISTS `"+ table +"` ("
                        + "`owner` int(11) unsigned NOT NULL,"
                        + "`x` int(11) NOT NULL,"
                        + "`y` int(11) NOT NULL,"
                        + "`z` int(11) NOT NULL,"
                        + "`type` int(11) unsigned NOT NULL,"
                        + "`allowed` varchar(255) DEFAULT NULL,"
                        + "`time` bigint(20) unsigned NOT NULL"
                    + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";

            Log.log("    - Creating table for world§8:§a " + name + "§f...");

            try {
                database.createTable(sql);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }

            try {
                database.commit(true);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        
        try {

            String indexname = name + "_block_search";
            if (!database.hasIndex(table, indexname)) {
                
                String index = "CREATE INDEX {exists} `"+ indexname +"` ON `"+table+"` (`x`,`z`,`y`);";
                Log.log("    - Creating index §8:§a " + indexname + "§f...");

                try {
                    database.createIndex(index);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                
                try {
                    database.commit(true);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }

        } catch (SQLException ex) {
            Log.error(ex, "Failed to check if table has index.");
        }
    }
    
    private void clean(String table, String name, SQLDb database, MainNodePack config) {
        
        if (database.hasTable(table)) {
            
            String end = ";";

            if (database.getEngineType() == CoreType.MySQL) {
                end = " ON `" + table + "`;";
            }
            
            try {
                database.createIndex("DROP INDEX `" + config.database_prefix + "block_" + name + "`" + end);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            
            try {
                database.createIndex("DROP INDEX `" + config.database_prefix + "owner_" + name + "`" + end);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            
            try {
                database.createIndex("DROP INDEX `" + config.database_prefix + "type_" + name + "`" + end);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
}