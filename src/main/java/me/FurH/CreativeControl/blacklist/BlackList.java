package me.FurH.CreativeControl.blacklist;

import java.util.HashSet;
import org.bukkit.Material;

/**
 *
 * @author FurmigaHumana
 * All Rights Reserved unless otherwise explicitly stated.
 */
public class BlackList {

    public boolean isBlackListed(Material m, byte data, HashSet<Object> set) {

        if (set.contains(m)) {
            return true;
        }
        
        if (data == 0) {
            return false;
        }

        String id = m.toString();

        if (set.contains(id + ":" + data)) {

            if (data == 0) {
                set.add(m);
            }

            return true;
        }

        return false;
    }
}