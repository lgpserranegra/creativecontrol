package me.FurH.CreativeControl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import me.FurH.Bukkit.Core.util.BCom;
import me.FurH.Core.close.Closer;
import me.FurH.Core.database.SQLDb;
import me.FurH.Core.database.SQLDb.CoreType;
import me.FurH.Core.debug.Log;
import me.FurH.Core.gc.IMemoryMonitor;
import me.FurH.Core.gc.MemoryMonitor;
import me.FurH.Core.number.NumberUtils;
import me.FurH.CreativeControl.blacklist.BlackList;
import me.FurH.CreativeControl.cmds.Commands;
import me.FurH.CreativeControl.config.MainConfig;
import me.FurH.CreativeControl.config.MainNodePack;
import me.FurH.CreativeControl.config.world.WorldConfig;
import me.FurH.CreativeControl.database.DatabaseManager;
import me.FurH.CreativeControl.inventory.InventoryManager;
import me.FurH.CreativeControl.listeners.BlockListener;
import me.FurH.CreativeControl.listeners.EntityListener;
import me.FurH.CreativeControl.listeners.MoveListener;
import me.FurH.CreativeControl.listeners.PlayerListener;
import me.FurH.CreativeControl.listeners.WorldListener;
import me.FurH.CreativeControl.manager.BlockManager;
import me.FurH.CreativeControl.manager.PlayerManager;
import me.FurH.CreativeControl.messages.Messages;
import me.FurH.CreativeControl.metrics.MetricsManager;
import me.FurH.CreativeControl.nodrop.BlockNoDrop;
import me.FurH.CreativeControl.permissions.CPerm;
import me.FurH.CreativeControl.region.RegionManager;
import me.FurH.CreativeControl.utils.Communicator;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.PluginManager;
import org.bukkit.scheduler.BukkitRunnable;

/**
 *
 * @author FurmigaHumana
 * All Rights Reserved unless otherwise explicitly stated.
 */
public class CreativeControl implements IMemoryMonitor {

    private final Communicator communicator;
    private final EntryPoint entry;
    
    private final MainConfig config;
    private final WorldConfig worlds;
    private final Messages messages;
    
    /* ** listeners ** */
    private final BlockListener     block_listener;
    private final EntityListener    entity_listener;
    private final MoveListener      move_listener;
    private final PlayerListener    player_listener;
    private final WorldListener     world_listener;
    
    /* ** blacklist ** */
    private final BlackList black_list;
    
    /* ** block manager ** */
    private final BlockManager block_manager;
    private final PlayerManager players;
    private final InventoryManager inv_manager;

    /* ** database ** */
    private SQLDb database;
    
    /* ** tasks ** */
    private final BukkitRunnable blockdrop;
    private final BlockNoDrop nodrop;
    
    /* **regions ** */
    private final RegionManager regions;
    private final MetricsManager metrics;
    private final DatabaseManager dbmanager;
    
    /* ** permissions ** */
    private final CPerm permissions;
    
    private boolean uuid_update = false;

    public CreativeControl(EntryPoint entrypoint) {

        this.entry = entrypoint;

        communicator = new Communicator();

        this.config = new MainConfig();
        this.worlds = new WorldConfig();
        this.messages = new Messages();
        
        this.permissions = new CPerm();
        
        this.black_list = new BlackList();
        this.block_manager = new BlockManager();
        this.metrics = new MetricsManager();
        
        this.dbmanager = new DatabaseManager();
        this.inv_manager = new InventoryManager();

        this.block_listener     = new BlockListener();
        this.entity_listener    = new EntityListener();
        this.move_listener      = new MoveListener();
        this.player_listener    = new PlayerListener();
        this.world_listener     = new WorldListener();
        
        this.players = new PlayerManager();
        this.regions = new RegionManager();

        this.nodrop = new BlockNoDrop();

        this.blockdrop = new BukkitRunnable() {
            @Override
            public void run() {
                nodrop.blockDrop();
            }
        };
    }
    
    public void initialize(long start) {
        
        BCom.set(communicator);
        Log .set(communicator);
        
        messages.load();
        
        final MainNodePack node = config.get();
        
        communicator.setDebug(node.com_debugcons);
        communicator.setQuiet(node.com_quiet);
        
        block_manager.initialize(node.cache_capacity);
                
        database = new SQLDb(node.threads, node.thread_speed, node.database_host, node.database_port, node.database_table, node.database_user, node.database_pass, node.database_type, node.sqlite_file, node.keepalive, true, node.timeout);

        dbmanager.initialize(database, node);
        regions.initialize();

        PluginManager pm = Bukkit.getPluginManager();

        /* ** register events ** */
        pm.registerEvents(block_listener,   entry);
        pm.registerEvents(entity_listener,  entry);
        pm.registerEvents(move_listener,    entry);
        pm.registerEvents(player_listener,  entry);
        pm.registerEvents(world_listener,   entry);

        /* ** commands ** */
        entry.getCommand("creativecontrol").setExecutor(new Commands());
        
        blockdrop.runTaskTimerAsynchronously(entry, 60, 60);

        try {
            //metrics.startMetrics();

            database.worker().statement(false);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        if (isUUID()) {
            Bukkit.getScheduler().runTaskAsynchronously(entry, new Runnable() {
                @Override
                public void run() {
                    
                    String table = node.database_prefix + "players";
                    
                    if (database.hasTable(table)) {

                        Statement st = null;
                        ResultSet rs = null;

                        try {

                            st = database.statement(false);
                            st.execute("SELECT * FROM `" + table + "` LIMIT 1;");
                            
                            rs = st.getResultSet();
                            
                            if (rs.next()) {
                                Log.info("  - Players table is not empty");
                                uuid_update = true;
                            }
                            
                        } catch (SQLException ex) {
                            Log.error(ex, "Failed to check if database need update");
                        } finally {
                            Closer.closeQuietly(rs, st);
                        }
                    }
                    
                    if (uuid_update) {
                        for (Player p : Bukkit.getOnlinePlayers()) {
                            updatePlayer(p);
                        }
                    }
                }
            });
        }

        Log.log(Messages.prefix + " {0} v{1} loaded in {2} ms", entry.getDescription().getName(), entry.getDescription().getVersion(), (System.currentTimeMillis() - start));
    }

    public void shutdown(long start) {

        HandlerList.unregisterAll(entry);

        /* ** unload settings ** */
        this.config.unload();
        
        /* ** unload world settings ** */
        this.worlds.unload();
        
        try {
            database.disconnect();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
        blockdrop.cancel();

        MemoryMonitor.get().shutdown();

        Log.log(Messages.prefix + " {0} v{1} disabled in {2} ms", entry.getDescription().getName(), entry.getDescription().getVersion(), (System.currentTimeMillis() - start));
    }

    public MainNodePack getMainNode() {
        return this.config.get();
    }
    
    public WorldConfig getWorldConfig() {
        return this.worlds;
    }

    public BlackList getBlackList() {
        return this.black_list;
    }

    public BlockManager getBlockManager() {
        return this.block_manager;
    }
    
    public SQLDb getDb() {
        return database;
    }
    
    public EntryPoint getEntryPoint() {
        return entry;
    }
    
    public PlayerListener getPlayerListener() {
        return this.player_listener;
    }

    public BlockNoDrop getNoDrop() {
        return this.nodrop;
    }

    public CPerm getPermissions() {
        return this.permissions;
    }
    
    public PlayerManager getPlayerManager() {
        return this.players;
    }
    
    public RegionManager getRegionManager() {
        return this.regions;
    }
    
    public EntityListener getEntityListener() {
        return this.entity_listener;
    }
    
    public InventoryManager getInvManager() {
        return this.inv_manager;
    }
    
    @Override
    public void gc() throws Throwable {

        Log.info(Messages.prefix + " Cleaning up...");
        long start = System.currentTimeMillis();

        block_listener  .gc();
        entity_listener .gc();
        player_listener .gc();
        world_listener  .gc();
        block_manager   .gc();
        players         .gc();
        inv_manager     .gc();
        database        .gc();
        nodrop          .gc();
        permissions     .gc();
        players         .gc();
        move_listener   .gc();

        Log.info(Messages.prefix + " Cleanup done in " + (System.currentTimeMillis() - start) + " ms");
    }

    @Override
    public boolean dispose() throws Throwable {
        return false; // nothing can be done here
    }

    @Override
    public void shutdown() throws Throwable {
        // nothing here
    }

    public void loadWorld(World world) {
        
        dbmanager.loadWorld(world.getName(), database, config.get());
        
        try {
            database.commit(true);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void release() {
        permissions     .release();
        block_manager   .release();
        world_listener  .release();
        player_listener .release();
        entity_listener .release();
        block_listener  .release();
        inv_manager     .release();
        players         .release();
        move_listener   .release();
    }

    public void reload() {
        config.unload();
        worlds.unload();
        messages.load();
    }

    public void cleanup(Player player) {
        permissions.cleanup(player);
        players.cleanup(player);
        entity_listener.cleanup(player);
        block_listener.cleanup(player);
        inv_manager.cleanup(player);
    }

    public void unload(World world) {
        worlds.unload(world);
    }
    
    public void updatePlayer(final Player p) {
        
        if (uuid_update) {
            Bukkit.getScheduler().runTaskAsynchronously(entry, new Runnable() {
                @Override
                public void run() {
                    
                    MainNodePack nodes = config.get();
                    
                    if (database.hasTable(nodes.database_prefix + "players")) {

                        Statement st1 = null;
                        Statement st2 = null;
                        Statement st3 = null;

                        ResultSet rs = null;

                        try {

                            st1 = database.statement(false);
                            st1.execute("SELECT id, player FROM `" + nodes.database_prefix + "players` WHERE `player` = '" + p.getName() + "' LIMIT 1;");

                            rs = st1.getResultSet();

                            if (rs.next()) {

                                st2 = database.statement(true);
                                int id = rs.getInt(1);

                                st2.execute("INSERT "+ (database.getEngineType() == CoreType.SQLite ? "OR " : "") +"IGNORE "
                                        + "INTO `" + nodes.database_prefix + "uuids` (id, uuid) VALUES ('" + id + "', '" + p.getUniqueId().toString() + "');");

                                if (st2.getUpdateCount() > 0) {
                                    st3 = database.statement(true);
                                    st3.execute("DELETE FROM `" + nodes.database_prefix + "players` WHERE `id` = '" + id + "' LIMIT 1;");
                                }

                                Log.info("  - Updated " + p.getName() + ", uuid: " + p.getUniqueId().toString() + ", id: " + id);
                            }

                        } catch (SQLException ex) {
                            Log.error(ex,"Failed to update " + p.getName() + " uuid record.");
                        } finally {
                            Closer.get().closeLater(rs, st1, st2, st3);
                        }
                    }
                }
            });
        }
    }
    
    public static boolean isUUID() {

        String version = Bukkit.getBukkitVersion();
        
        try {
            return NumberUtils.toDouble(version.substring(0, version.indexOf('.', version.indexOf('.') + 1))) > 1.6;
        } catch (Throwable ex) {
            return false;
        }
    }
}