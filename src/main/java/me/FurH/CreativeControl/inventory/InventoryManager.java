package me.FurH.CreativeControl.inventory;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Iterator;
import me.FurH.Core.cache.LimitedSizeMap;
import me.FurH.Core.close.Closer;
import me.FurH.Core.database.SQLDb;
import me.FurH.CreativeControl.EntryPoint;
import me.FurH.CreativeControl.config.MainNodePack;
import me.FurH.CreativeControl.manager.PlayerManager;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;

/**
 *
 * @author FurmigaHumana
 * All Rights Reserved unless otherwise explicitly stated.
 */
public class InventoryManager {
    
    private final LimitedSizeMap<String, InventoryData> cache;
        
    private PlayerManager _players;
    private SQLDb _database;
    private MainNodePack _config;
    
    public InventoryManager() {
        cache = new LimitedSizeMap(Bukkit.getMaxPlayers() * (GameMode.values().length + 1), new HashMap<String, InventoryData>());
    }

    public boolean process(Player p, GameMode newgm, GameMode oldgm) throws Exception {
        
        if (save(p, oldgm)) {
            return restore(p, newgm);
        }
        
        return false;
    }

    private boolean save(Player p, GameMode oldgm) throws Exception {
        
        if (_database == null) {
            _database = EntryPoint.get().getDb();
        }
        
        if (_players == null) {
            _players =  EntryPoint.get().getPlayerManager();
        }
        
        if (_config == null) {
            _config = EntryPoint.get().getMainNode();
        }

        String table = _config.database_prefix + "inventory_" + oldgm.toString().toLowerCase();

        InventoryData data = new InventoryData(p);

        String armor = data.armorString();
        String contents = data.invString();
        String status = data.serialize();

        cache.put(p.getName() + "_" + oldgm.toString(), data);

        String sql = "INSERT INTO `" + table + "` (player, status, armor, contents) VALUES ('" + _players.playerId(p) + "', '" + status + "', '" + armor + "', '" + contents + "') "
                + "ON DUPLICATE KEY UPDATE status = '" + status + "', armor = '" + armor + "' , contents = '" + contents + "';";
        
        Statement st = null;
        
        try {
        
            st = _database.statement(true);
            st.execute(sql);
    
            _database.commit(false);

            return st.getUpdateCount() > 0;

        } finally {
            Closer.get().closeLater(st);
        }
    }

    private boolean restore(Player p, GameMode newgm) throws Exception {

        if (_database == null) {
            _database = EntryPoint.get().getDb();
        }
        
        if (_players == null) {
            _players =  EntryPoint.get().getPlayerManager();
        }
        
        if (_config == null) {
            _config = EntryPoint.get().getMainNode();
        }

        String table = _config.database_prefix + "inventory_" + newgm.toString().toLowerCase();
        InventoryData data = cache.get(p.getName() + "_" + newgm.toString());

        if (data != null) {
            data.restore(p); return true;
        }

        String sql = "SELECT status, armor, contents FROM `" + table + "` WHERE `player` = '" + _players.playerId(p) + "' LIMIT 1;";
        ResultSet rs = null;

        try {
           
            rs = _database.cached(sql, false);
                        
            if (rs.next()) {
                data = new InventoryData(rs.getString(1), rs.getString(2), rs.getString(3));
                data.restore(p);
            }

        } finally {
            Closer.get().closeLater(rs);
        }
        
        return true;
    }
    
    public void gc() throws Throwable {
        cache.clear();
        release();
    }
    
    public void release() {
        _players    = null;
        _database   = null;
        _config     = null;
    }
    
    public void cleanup(Player p) {

        Iterator<String> it = cache.keySet().iterator();
        
        while (it.hasNext()) {
            if (it.next().toLowerCase().startsWith(p.getName().toLowerCase())) {
                it.remove();
            }
        }
    }
}
