package me.FurH.CreativeControl.inventory;

import me.FurH.Bukkit.Core.inventory.BInventoryStack;
import me.FurH.Core.object.ObjectUtils;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.configuration.file.YamlConstructor;
import org.bukkit.configuration.file.YamlRepresenter;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.DumperOptions.FlowStyle;
import org.yaml.snakeyaml.DumperOptions.LineBreak;
import org.yaml.snakeyaml.DumperOptions.ScalarStyle;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.representer.Representer;

/**
 *
 * @author FurmigaHumana
 * All Rights Reserved unless otherwise explicitly stated.
 */
public final class InventoryData {

    private final ItemStack[]   contents;
    private final ItemStack[]   armors;
    private final String        data;

    public InventoryData(String data, String armor, String items) throws Exception {
        this.armors = BInventoryStack.getArrayFromString(armor);
        this.contents = BInventoryStack.getArrayFromString(items);
        this.data = data;
    }

    public InventoryData(Player p) {
        
        this.contents = p.getInventory().getContents();

        YamlConfiguration config = new YamlConfiguration();
        
        config.set("ex", p.getExp());
        config.set("lv", p.getLevel());
        config.set("tx", p.getTotalExperience());
        config.set("eh", p.getExhaustion());
        config.set("st", p.getSaturation());
        config.set("fl", p.getFoodLevel());
        config.set("af", p.getAllowFlight());
        config.set("if", p.isFlying());
        config.set("dt", p.getNoDamageTicks());
        config.set("fs", p.getFlySpeed());
        config.set("ws", p.getWalkSpeed());
        config.set("hd", p.isHealthScaled());
        config.set("hs", p.getHealthScale());
        config.set("h", p.getHealth());
        config.set("ft", p.getFireTicks());
        config.set("ra", p.getRemainingAir());

        this.data = config.saveToString();
        
        this.armors = p.getInventory().getArmorContents();
    }
    
    public String serialize() throws InvalidConfigurationException {

        YamlConfiguration config = new YamlConfiguration();
        config.loadFromString(data);

        DumperOptions yamlOptions = new DumperOptions();
        Representer yamlRepresenter = new YamlRepresenter();

        Yaml yaml = new Yaml(new YamlConstructor(), yamlRepresenter, yamlOptions);

        yamlOptions.setIndent(1);
        yamlOptions.setLineBreak(LineBreak.UNIX);
        yamlOptions.setDefaultScalarStyle(ScalarStyle.SINGLE_QUOTED);
        yamlOptions.setDefaultFlowStyle(FlowStyle.AUTO);

        return yaml.dump(config.getValues(true));
    }

    public String armorString() throws Exception {
        return BInventoryStack.getStringFromArray(armors);
    }

    public String invString() throws Exception {
        return BInventoryStack.getStringFromArray(contents);
    }

    public void restore(Player p) throws InvalidConfigurationException {

        p.getInventory().setArmorContents(armors);
        p.getInventory().setContents(contents);

        YamlConfiguration config = new YamlConfiguration();
        config.loadFromString(data);

        p.setExp(ObjectUtils.toFloat(config.get("ex", p.getExp())));
        p.setLevel(config.getInt("lv", p.getLevel()));
        p.setTotalExperience(config.getInt("tx", p.getTotalExperience()));
        p.setExhaustion(ObjectUtils.toFloat(config.get("eh", p.getExhaustion())));
        p.setSaturation(ObjectUtils.toFloat(config.get("st", p.getSaturation())));
        p.setFoodLevel(config.getInt("fl", p.getFoodLevel()));
        p.setAllowFlight(config.getBoolean("af", p.getAllowFlight()));
        p.setFlying(config.getBoolean("if", p.isFlying()));
        p.setNoDamageTicks(config.getInt("dt", p.getNoDamageTicks()));
        p.setFlySpeed(ObjectUtils.toFloat(config.get("fs", p.getFlySpeed())));
        p.setWalkSpeed(ObjectUtils.toFloat(config.get("ws", p.getWalkSpeed())));
        p.setHealthScaled(config.getBoolean("hd", p.isHealthScaled()));
        p.setHealthScale(config.getDouble("hs", p.getHealthScale()));
        p.setHealth(config.getDouble("h", p.getHealth()));
        p.setFireTicks(config.getInt("ft", p.getFireTicks()));
        p.setRemainingAir(config.getInt("ra", p.getRemainingAir()));
        
        p.saveData(); p.updateInventory();
    }
}