package me.FurH.CreativeControl;

import me.FurH.Core.gc.MemoryMonitor;
import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author FurmigaHumana
 * All Rights Reserved unless otherwise explicitly stated.
 */
public class EntryPoint extends JavaPlugin {
    
    private static CreativeControl instance;
    private static final long startup;
    
    static {
        startup = System.currentTimeMillis();
    }

    @Override
    public void onEnable() {
                
        /*  ** Load Classes ** */
        instance = new CreativeControl(this);
        
        MemoryMonitor.get().register(instance);
        
        /* ** Initialize Everything ** */
        instance.initialize(startup);
        
        /* log enabled */
    }
    
    @Override
    public void onDisable() {
        
        long start = System.currentTimeMillis();
        
        /* ** Shutdown Everything ** */
        instance.shutdown(start);

        /* ** Target for garbage collection */
        instance = null;

        /* log disabled */
    }
    
    public static CreativeControl get() {
        return instance;
    }
    
    public static EntryPoint getPlugin() {
        return instance.getEntryPoint();
    }

    public void reload() {
        onDisable();
        onEnable();
    }
}
