package me.FurH.CreativeControl.cmds.execute;

import java.io.File;
import me.FurH.Bukkit.Core.configuration.BConfigLoader;
import me.FurH.Bukkit.Core.util.BCom;
import me.FurH.Core.debug.Log;
import me.FurH.CreativeControl.EntryPoint;
import me.FurH.CreativeControl.config.MainConfig;
import me.FurH.CreativeControl.messages.Messages;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author FurmigaHumana
 * All Rights Reserved unless otherwise explicitly stated.
 */
public class SetCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {

        if (!(sender instanceof Player)) {
            Log.log("§cThis command can't be used here"); return true;
        }
        
        Player p = (Player) sender;

        ItemStack helmet        = p.getInventory().getHelmet();
        ItemStack chestplate    = p.getInventory().getChestplate();
        ItemStack leggings      = p.getInventory().getLeggings();
        ItemStack boots         = p.getInventory().getBoots();

        JavaPlugin plugin = EntryPoint.getPlugin();
        
        File input = new File(plugin.getDataFolder(), "settings.yml");
        BConfigLoader loader = new BConfigLoader(plugin, "settings.yml", input);
        
        Log.log(Messages.prefix + " Loading file: " + input.getName() + "...");
        
        try {
            loader.load();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
        if (helmet != null) {
            loader.set("CreativeArmor.Helmet", helmet);
        }
        
        if (chestplate != null) {
            loader.set("CreativeArmor.Chestplate", chestplate);
        }
        
        if (leggings != null) {
            loader.set("CreativeArmor.Leggings", leggings);
        }
        
        if (boots != null) {
            loader.set("CreativeArmor.Boots", boots);
        }

        try {
            loader.save(input);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        BCom.msg(sender, "§aCreative armor set!");
        
        return true;
    }
}