package me.FurH.CreativeControl.cmds.execute;

import me.FurH.Bukkit.Core.util.BCom;
import me.FurH.Core.debug.Log;
import me.FurH.CreativeControl.EntryPoint;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/**
 *
 * @author FurmigaHumana
 * All Rights Reserved unless otherwise explicitly stated.
 */
public class ReloadCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {
        
        if (args.length == 2) {
            
            String arg = args[ 1 ];
            
            if (arg.equalsIgnoreCase("config")) {
                
                BCom.msg(sender, "§7Reloading...");
            
                EntryPoint.get().reload();
                BCom.msg(sender, "§aConfiguration files reloaded!");
            
            } else if (arg.equalsIgnoreCase("cache")) {
                
                BCom.msg(sender, "§7Reloading...");
               
                try {
                    
                    EntryPoint.get().gc();
                    BCom.msg(sender, "§aCaches reloaded!");
                    
                } catch (Throwable ex) {
                    BCom.msg(sender, "§cFailed to flush caches: " + ex.getMessage());
                    Log.error(ex, "Error on command /cc reload cache");
                }
                
                
            } else if (arg.equalsIgnoreCase("plugin")) {
                
                BCom.msg(sender, "§7Reloading...");
                
                EntryPoint.getPlugin().reload();
                BCom.msg(sender, "§aPlugin reinitialized!");
                
            }
            
            return true;
        }

        BCom.msg(sender, "§a/cc reload config §8-§7 Reload config files");
        BCom.msg(sender, "§a/cc reload cache §8-§7 Flush caches");
        BCom.msg(sender, "§a/cc reload plugin §8-§7 Reinitialize the plugin");

        return true;
    }
}
