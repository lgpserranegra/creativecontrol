package me.FurH.CreativeControl.cmds.execute;

import me.FurH.Bukkit.Core.util.BCom;
import me.FurH.CreativeControl.CreativeControl;
import me.FurH.CreativeControl.EntryPoint;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/**
 *
 * @author FurmigaHumana
 * All Rights Reserved unless otherwise explicitly stated.
 */
public class StatusCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {
        
        BCom.msg(sender, "§aLoading data...");
        EntryPoint plugin = EntryPoint.getPlugin();
        
        Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
            @Override
            public void run() {
                
                CreativeControl plugin = EntryPoint.get();
                // @TODO
                
            }
        });
        
        return true;
    }
}
