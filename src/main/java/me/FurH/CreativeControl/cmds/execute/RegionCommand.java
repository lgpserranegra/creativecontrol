package me.FurH.CreativeControl.cmds.execute;

import me.FurH.Bukkit.Core.util.BCom;
import me.FurH.CreativeControl.CreativeControl;
import me.FurH.CreativeControl.EntryPoint;
import me.FurH.CreativeControl.listeners.PlayerListener;
import me.FurH.CreativeControl.region.RegionManager;
import me.FurH.CreativeControl.selection.AreaSelection;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author FurmigaHumana
 * All Rights Reserved unless otherwise explicitly stated.
 */
public class RegionCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {
        
        if (!(sender instanceof Player)) {
            return true; // @TODO NOTIFY
        }

        Player p = (Player) sender;

        if (args.length == 4) {
            if (args[ 1 ].equalsIgnoreCase("define")) {
                // /cc region[0] define[1] <type>[2] <name>[3]
                return create(p, args);
            }
        }

        if (args.length == 3) {
            if (args[ 1 ].equalsIgnoreCase("remove")) {
                // /cc region[0] remove[1] <name>[2]
                return remove(p, args);
            }
        }

        BCom.msg(sender, "§a/cc region define creative <name> §8-§7 Create a creative region");
        BCom.msg(sender, "§a/cc region define survival <name> §8-§7 Create a survival region");
        BCom.msg(sender, "§a/cc region remove <name> §8-§7 Remove a region");
        
        return true;
    }
    
    private boolean remove(Player p, String[] args) {

        CreativeControl plugin  = EntryPoint.get();
        RegionManager manager = plugin.getRegionManager();

        String name = args[2];

        
        manager.remove(name);
        BCom.msg(p, "Region removed");
        
        return true;
    }
    
    private boolean create(Player p, String[] args) {

        CreativeControl plugin  = EntryPoint.get();
        PlayerListener listener = plugin.getPlayerListener();

        AreaSelection selection = null;

        try {
            selection = listener.getSelection(p.getName());
        } catch (Exception ex) {
            BCom.msg(p, "§c" + ex.getMessage());
        }
        
        if (selection == null) {
            BCom.msg(p, "Select the area first.");
        }

        GameMode type = GameMode.valueOf(args[2].toUpperCase());

        if (type != null) {

            RegionManager manager = plugin.getRegionManager();
            String name = args[ 3 ];

            if (!manager.has(name)) {

                manager.create(name, type, selection);
                BCom.msg(p, "§c{0} §fregion created successfully!", type.toString().toLowerCase());

                return true;
            }
            
            BCom.msg(p, "chooser another name");
            
        } else {
            BCom.msg(p, "§c{0} is not a valid gamemode!", args[2]);
        }

        return true;
    }
}
