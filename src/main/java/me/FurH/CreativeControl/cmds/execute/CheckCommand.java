package me.FurH.CreativeControl.cmds.execute;

import me.FurH.Bukkit.Core.util.BCom;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author FurmigaHumana
 * All Rights Reserved unless otherwise explicitly stated.
 */
public class CheckCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {

        if (args.length == 3) {
            if (args[ 1 ].equalsIgnoreCase("player")) {

                Player player = Bukkit.getPlayer(args[2]);

                if (player != null) {
                    BCom.msg(sender, "§7{0} has §a{1} gamemode", player.getName(), player.getGameMode().toString().toLowerCase());
                } else {
                    BCom.msg(sender, "§7{0} §ais not§7 online!", args[2]);
                }

                return true;
            }
        }

        if (args.length == 2) {

            if (args[ 1 ].equalsIgnoreCase("status")) {

                int creative = 0;
                int survival = 0;
                int adventure = 0;

                for (Player player : Bukkit.getOnlinePlayers()) {
                    
                    GameMode gm = player.getGameMode();
                    
                    switch (gm) {
                        case CREATIVE: {
                            creative++;
                        }
                        case SURVIVAL: {
                            survival++;
                        }
                        case ADVENTURE: {
                            adventure++;
                        }
                    }
                }

                BCom.msg(sender, "§7There are§8:§7");

                BCom.msg(sender, "§8    - §7 CREATIVE§8: §a" + creative + "§a7 player" + (creative > 1 ? "s" : ""));
                BCom.msg(sender, "§8    - §7 SURVIVAL§8: §a" + survival + "§a7 player" + (survival > 1 ? "s" : ""));
                BCom.msg(sender, "§8    - §7 ADVENTURE§8: §a" + adventure + "§a7 player" + (adventure > 1 ? "s" : ""));

                return true;
            }
        }

        BCom.msg(sender, "§a/cc check status §8-§7 Check the player gamemodes");
        BCom.msg(sender, "§a/cc check player <player> §8-§7 Get the player gamemode");
        
        return true;
    }
}