package me.FurH.CreativeControl.cmds;

import me.FurH.Bukkit.Core.util.BCom;
import me.FurH.CreativeControl.EntryPoint;
import me.FurH.CreativeControl.cmds.execute.CheckCommand;
import me.FurH.CreativeControl.cmds.execute.RegionCommand;
import me.FurH.CreativeControl.cmds.execute.ReloadCommand;
import me.FurH.CreativeControl.cmds.execute.SetCommand;
import me.FurH.CreativeControl.cmds.execute.StatusCommand;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/**
 *
 * @author FurmigaHumana
 * All Rights Reserved unless otherwise explicitly stated.
 */
public class Commands implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {
        
        if (args.length <= 0) {
            BCom.msg(sender, "§8[§aCreativeControl§8]§f: §8CreativeControl §a{0} §8by §aFurmigaHumana", EntryPoint.getPlugin().getDescription().getVersion());
            BCom.msg(sender, "§8[§aCreativeControl§8]§f: Type '§a/cc help§f' to see the command list");
        }

        CommandExecutor executor = null;
        String arg = args[ 0 ];

        if (arg.equalsIgnoreCase("reload")) {
            executor = new ReloadCommand();
        } else if (arg.equalsIgnoreCase("status")) {
            executor = new StatusCommand();
        } else if (arg.equalsIgnoreCase("region")) {
            executor = new RegionCommand();
        } else if (arg.equalsIgnoreCase("check")) {
            executor = new CheckCommand();
        } else if (arg.equalsIgnoreCase("setarmor")) {
            executor = new SetCommand();
        }
        
        if (executor != null) {
            executor.onCommand(sender, cmd, string, args); return true;
        }
        
        //BCom.msg(sender, "§a/cc tool <add/del> §8-§7 Manualy unprotect/protect blocks");
        BCom.msg(sender, "§a/cc status §8-§7 Simple cache and database status");
        //BCom.msg(sender, "§a/cc <add/del> §8-§7 Protect/unprotect blocks inside the selection");
        //BCom.msg(sender, "§a/cc admin migrator §8-§7 Migrate the database to others types");
        BCom.msg(sender, "§a/cc check <status/player> §8-§7 Get player gamemode data");
        //BCom.msg(sender, "§a/cc cleanup <all/type/player/world/corrupt> §8-§7 Clean the database");
        BCom.msg(sender, "§a/cc region <create/remove> §8-§7 Create or remove gamemode regions");
        //BCom.msg(sender, "§a/cc sel expand <up/down/ver> §8-§7 Expand the current selection");
        //BCom.msg(sender, "§a/cc friend <add/remove/list/allow/transf> §8-§7 Friend list manager");
        BCom.msg(sender, "§a/cc setarmor §8-§7 Set the creative armor");
        BCom.msg(sender, "§a/cc reload §8-§7 Full reload of the plugin");
        
        return true;
    }
}
