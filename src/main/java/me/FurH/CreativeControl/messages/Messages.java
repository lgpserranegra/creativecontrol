package me.FurH.CreativeControl.messages;

import java.io.File;
import me.FurH.Bukkit.Core.configuration.BConfigLoader;
import me.FurH.Core.debug.Log;
import me.FurH.Core.object.ObjectUtils;
import me.FurH.CreativeControl.EntryPoint;
import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author FurmigaHumana
 * All Rights Reserved unless otherwise explicitly stated.
 */
public class Messages {

    public static final String prefix = "§8[§aCreativeControl§8]§7:§f ";

    public static String mainode_restricted                = "MainNode.restricted";
    
    public static String blockplace_cantplace              = "BlockPlace.cantplace";

    public static String blockmanager_belongs              = "BlockManager.belongsto";
    public static String blockmanager_limit                = "BlockManager.minutelimit";

    public static String blockbreak_cantbreak              = "BlockBreak.cantbreak";
    public static String blockbreak_survival               = "BlockBreak.nosurvival";
    public static String blockbreak_creativeblock          = "BlockBreak.creativeblock";

    public static String limits_vehicles                   = "Limits.vehicles";

    public static String region_welcome                    = "Regions.welcome";
    public static String region_farewell                   = "Regions.farewell";
    public static String region_unallowed                  = "Regions.survival";
    public static String region_cant_change                = "Regions.cant_here";

    public static String blacklist_commands                = "BlackList.commands";

    public static String selection_first                   = "Selection.first_point";
    public static String selection_second                  = "Selection.second_point";

    public void load() {

        long start = System.currentTimeMillis();
        
        JavaPlugin plugin = EntryPoint.getPlugin();
        
        File input = new File(plugin.getDataFolder(), "messages.yml");
        BConfigLoader loader = new BConfigLoader(plugin, "messages.yml", input);

        Log.log(prefix + " Loading file: " + input.getName() + "...");

        try {
            loader.load();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        mainode_restricted                = getMessage(loader, "MainNode.restricted");
    
        blockplace_cantplace              = getMessage(loader, "BlockPlace.cantplace");
    
        blockmanager_belongs              = getMessage(loader, "BlockManager.belongsto");
        blockmanager_limit                = getMessage(loader, "BlockManager.minutelimit");

        blockbreak_cantbreak              = getMessage(loader, "BlockBreak.cantbreak");
        blockbreak_survival               = getMessage(loader, "BlockBreak.nosurvival");
        blockbreak_creativeblock          = getMessage(loader, "BlockBreak.creativeblock");
    
        limits_vehicles                   = getMessage(loader, "Limits.vehicles");
    
        region_welcome                    = getMessage(loader, "Regions.welcome");
        region_farewell                   = getMessage(loader, "Regions.farewell");
        region_unallowed                  = getMessage(loader, "Regions.unallowed");
        region_cant_change                = getMessage(loader, "Regions.cant_here");
    
        blacklist_commands                = getMessage(loader, "BlackList.commands");
    
        selection_first                   = getMessage(loader, "Selection.first_point");
        selection_second                  = getMessage(loader, "Selection.second_point");

        try {
            loader.save(input);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        Log.log(prefix + " File " + input.getName() + " loaded in " + (System.currentTimeMillis() - start) + " ms");
    }

    private String getMessage(BConfigLoader loader, String key) {
        return ObjectUtils.toString(loader.get(key, key));
    }
}