package me.FurH.CreativeControl.nodrop;

import java.util.Collection;
import java.util.HashSet;
import java.util.concurrent.TimeUnit;
import me.FurH.Core.time.TimeUtils;
import org.bukkit.Material;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author FurmigaHumana
 * All Rights Reserved unless otherwise explicitly stated.
 */
public class BlockDropData {

    private final HashSet<Material> types;    
    private final long created;

    public BlockDropData(Collection<ItemStack> drops) {

        created = System.currentTimeMillis();
        types = new HashSet<Material>();

        for (ItemStack stack : drops) {
            types.add(stack.getType());
        }
    }
    
    public boolean expired() {
        return TimeUtils.isExpired(created, 5, TimeUnit.SECONDS);
    }
    
    public boolean gc() {

        types.clear();

        return TimeUtils.isExpired(created, 3, TimeUnit.SECONDS);
    }

    public boolean has(Item item) {

        if (types.isEmpty()) {
            return true;
        }

        return types.contains(item.getItemStack().getType());
    }
}