package me.FurH.CreativeControl.nodrop;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author FurmigaHumana
 * All Rights Reserved unless otherwise explicitly stated.
 */
public class BlockNoDrop {

    private final Map<Location, BlockDropData> locations;

    public BlockNoDrop() {
        locations = new HashMap<Location, BlockDropData>();
    }

    public boolean isDropRemoved(Location loc, Item item) {

        BlockDropData data = locations.get(loc);

        if (data == null) {
            return false;
        }

        if (data.expired()) {
            locations.remove(loc); return false;
        }

        return data.has(item);
    }

    public void removeDrop(Block b, ItemStack tool) {
        locations.put(b.getLocation(), new BlockDropData(tool == null ? b.getDrops() : b.getDrops(tool)));
    }

    public void gc() {

        Iterator<BlockDropData> it = locations.values().iterator();

        while (it.hasNext()) {
            if (it.next().gc()) {
                it.remove();
            }
        }

        if (locations.isEmpty()) {
            locations.clear();
        }
    }
    
    public void blockDrop() {

        Iterator<BlockDropData> it = locations.values().iterator();
        
        while (it.hasNext()) {
            if (it.next().expired()) {
                it.remove();
            }
        }
        
        if (locations.isEmpty()) {
            locations.clear();
        }
    }
}
