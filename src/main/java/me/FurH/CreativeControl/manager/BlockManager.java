package me.FurH.CreativeControl.manager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import me.FurH.Bukkit.Core.location.BLocationUtils;
import me.FurH.Core.cache.LimitedSizeMap;
import me.FurH.Core.close.Closer;
import me.FurH.Core.database.SQLDb;
import me.FurH.Core.database.SQLWorker;
import me.FurH.CreativeControl.EntryPoint;
import me.FurH.CreativeControl.config.MainNodePack;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

/**
 *
 * @author FurmigaHumana
 * All Rights Reserved unless otherwise explicitly stated.
 */
public class BlockManager {

    private LimitedSizeMap<String, String> cache;

    private MainNodePack _config;
    private SQLDb _database;
    private PlayerManager _players;

    public void initialize(int sizelimit) {
        cache = new LimitedSizeMap<String, String>(sizelimit, new HashMap<String, String>());
    }
    
    public void unprotect(final Block b) {
        
        Location loc = b.getLocation();
        
        final int x = loc.getBlockX();
        final int y = loc.getBlockY();
        final int z = loc.getBlockZ();
        
        final String world = b.getWorld().getName();

        String key = BLocationUtils.getKeyFromLocation(world, x, y, z);

        if (_database == null) {
            _database = EntryPoint.get().getDb();
        }

        if (_config == null) {
            _config = EntryPoint.get().getMainNode();
        }
        
        final SQLWorker worker = _database.worker();

        worker.enqueue(new Runnable() {
            @Override
            public void run() {
                
                Statement st = null;

                try {

                    st = worker.statement(true);
                    st.execute("DELETE FROM `" + _config.database_prefix + "blocks_" + world +"` WHERE `x` = '" + x + "' AND `z` = '" + z + "' AND `y` = '" + y + "';");

                } catch (SQLException ex) {
                    ex.printStackTrace();
                } finally {
                    Closer.closeQuietly(st);
                }
            }
        });

        cache.remove(key);
    }

    public boolean protect(final Player p, Block b) {

        Location loc = b.getLocation();

        final int x = loc.getBlockX();
        final int y = loc.getBlockY();
        final int z = loc.getBlockZ();

        final String world = b.getWorld().getName();

        String key = BLocationUtils.getKeyFromLocation(world, x, y, z);
        cache.put(key, p.getName());

        final int type = b.getType().getId();

        if (_database == null) {
            _database = EntryPoint.get().getDb();
        }
        
        if (_config == null) {
            _config = EntryPoint.get().getMainNode();
        }
        
        if (_players == null) {
            _players =  EntryPoint.get().getPlayerManager();
        }
        
        final SQLWorker worker = _database.worker();
        
        worker.enqueue(new Runnable() {
            @Override
            public void run() {
                
                Statement st = null;

                try {

                    st = worker.statement(true);
                    st.execute("INSERT INTO `" + _config.database_prefix + "blocks_" + world +"` (owner, x, y, z, type, allowed, time) VALUES ('" + _players.playerId(p) + "', '" + x + "', '" + y + "', '" + z + "', '" + type + "', '', '" + System.currentTimeMillis() + "');");

                } catch (SQLException ex) {
                    ex.printStackTrace();
                } finally {
                    Closer.closeQuietly(st);
                }
            }
        });

        return true;
    }

    public boolean isProtected(Block b) {

        Location loc = b.getLocation();

        int x = loc.getBlockX();
        int y = loc.getBlockY();
        int z = loc.getBlockZ();

        String world = b.getWorld().getName();
        String key = BLocationUtils.getKeyFromLocation(world, x, y, z);

        String name = cache.get(key);

        if (name != null) {
            return true;
        }

        final int type = b.getType().getId();
        ResultSet rs = null;

        try {

            if (_config == null) {
                _config = EntryPoint.get().getMainNode();
            }
            
            if (_database == null) {
                _database = EntryPoint.get().getDb();
            }
        
            String sql = "SELECT type FROM `" + _config.database_prefix + "blocks_" + world +"` WHERE `x` = '" + x + "' AND `z` = '" + z + "' AND `y` = '" + y + "' LIMIT 1;";
            rs = _database.cached(sql, false);

            if (rs.next()) {

                int dbtype = rs.getInt(1);

                if (type != dbtype) {
                    unprotect(b); return false;
                }
                
                return true;
            }

        } catch (SQLException ex) {
            ex.printStackTrace(); return true;
        } finally {
            Closer.get().closeLater(rs);
        }
        
        return false;
    }


    public void gc() throws Throwable {

        cache.clear();

        release();
    }

    public void release() {
        _config     = null;
        _database   = null;
        _players    = null;
    }

    public int size() {
        return cache.size();
    }
}