package me.FurH.CreativeControl.manager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import me.FurH.Core.cache.LimitedSizeMap;
import me.FurH.Core.close.Closer;
import me.FurH.Core.database.SQLDb;
import me.FurH.Core.debug.Log;
import me.FurH.CreativeControl.CreativeControl;
import me.FurH.CreativeControl.EntryPoint;
import me.FurH.CreativeControl.config.MainNodePack;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 *
 * @author FurmigaHumana
 * All Rights Reserved unless otherwise explicitly stated.
 */
public class PlayerManager {
    
    private final LimitedSizeMap<String, Integer> cache;
    
    private MainNodePack _config;
    private SQLDb _database;
    
    private final boolean uuids;

    public PlayerManager() {
        cache = new LimitedSizeMap<String, Integer>(Bukkit.getMaxPlayers() * 2, new HashMap<String, Integer>());
        this.uuids = CreativeControl.isUUID();
    }

    public int playerId(Player p) {

        String name = (uuids ? p.getUniqueId().toString() : p.getName());
        Integer cached = cache.get(name);

        if (cached != null) {
            return cached;
        }
        
        if (_database == null) {
            _database = EntryPoint.get().getDb();
        }
        
        if (_config == null) {
            _config = EntryPoint.get().getMainNode();
        }
        
        String sql = "SELECT `id` FROM `" + _config.database_prefix + (uuids ? "uuids" : "players") + "` WHERE `player` = '" + name + "' LIMIT 1;";
        
        Statement st = null;
        ResultSet rs = null;
        
        Statement st2 = null;
        
        try {
            
            st = _database.statement(false);
            st.execute(sql);
            
            rs = st.getResultSet();
            
            if (rs.next()) {
                
                int id = rs.getInt(1);

                cache.put(name, id);

                return id;
            }
            
            sql = "INSERT INTO `" + _config.database_prefix + (uuids ? "uuids" : "players") + "` (" + (uuids ? "uuid" : "player") + ") VALUES ('" + name + "');";

            st2 = _database.statement(true);
            st2.execute(sql);
            
            _database.commit(false);

            return playerId(p);
            
        } catch (SQLException ex) {
            Log.error(ex, "Failed to get " + name + "'s id");
        } finally {
            Closer.get().closeLater(rs, st, st2);
        }

        throw new RuntimeException("Failed to retrive player id from database");
    }

    public void gc() {
        
        cache.clear();
        
        release();
    }
    
    public void release() {
        _database = null;
        _config = null;
    }

    public void cleanup(Player p) {
        cache.remove(p.getName());
    }
}
