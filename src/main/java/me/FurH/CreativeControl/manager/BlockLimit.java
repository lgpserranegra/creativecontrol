package me.FurH.CreativeControl.manager;

/**
 *
 * @author FurmigaHumana
 * All Rights Reserved unless otherwise explicitly stated.
 */
public class BlockLimit {
    
    private long expire;
    private int placed = 0;
    
    public BlockLimit() {
        expire = System.currentTimeMillis() + 60000;
    }
    
    public boolean expired() {
        return expire < System.currentTimeMillis();
    }
    
    public void increment() {
        placed++;
    }
    
    public int placed() {
        return placed;
    }

    public void reset() {
        expire = System.currentTimeMillis() + 60000;
        placed = 0;
    }
}