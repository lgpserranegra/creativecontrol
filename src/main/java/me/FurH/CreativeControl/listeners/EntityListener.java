package me.FurH.CreativeControl.listeners;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;
import java.util.UUID;
import me.FurH.Bukkit.Core.util.BCom;
import me.FurH.CreativeControl.CreativeControl;
import me.FurH.CreativeControl.EntryPoint;
import me.FurH.CreativeControl.config.world.WorldConfig;
import me.FurH.CreativeControl.config.world.WorldNodePack;
import me.FurH.CreativeControl.manager.BlockManager;
import me.FurH.CreativeControl.messages.Messages;
import me.FurH.CreativeControl.nodrop.BlockNoDrop;
import me.FurH.CreativeControl.permissions.CPerm;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.entity.Vehicle;
import org.bukkit.entity.Villager;
import org.bukkit.entity.minecart.PoweredMinecart;
import org.bukkit.entity.minecart.StorageMinecart;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.entity.ExplosionPrimeEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.vehicle.VehicleCreateEvent;
import org.bukkit.event.vehicle.VehicleDestroyEvent;
import org.bukkit.metadata.FixedMetadataValue;

/**
 *
 * @author FurmigaHumana
 * All Rights Reserved unless otherwise explicitly stated.
 */
public class EntityListener implements Listener {

    private final HashMap<String, HashSet<UUID>> limits = new HashMap<String, HashSet<UUID>>();
    private final TreeSet<Player> waiting = new TreeSet<Player>();

    private WorldConfig   _config;
    private BlockManager  _manager;
    private BlockNoDrop   _nodrop;
    private CPerm         _perm;

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = false)
    public void onVehicleCreate(VehicleCreateEvent e) {
        
        if (waiting.isEmpty()) {
            return;
        }

        Player p = waiting.first();
        
        if (p == null) {
            return;
        }
        
        if (_config == null) {
            _config = EntryPoint.get().getWorldConfig();
        }
        
        World w = p.getWorld();
        
        WorldNodePack nodes = _config.get(w.getName());
        
        if (nodes.world_exclude) {
            return;
        }
        
        if (nodes.prevent_vehicle) {
            
            if (_perm == null) {
                _perm = EntryPoint.get().getPermissions();
            }
            
            if (!_perm.hasPerm(p, "Preventions.Vehicle")) {
                return;
            }
            
            int limit = nodes.prevent_limitvechile;
            
            if (limit <= 0) {
                return;
            }
            
            HashSet<UUID> entities = new HashSet<UUID>();
            String name = p.getName();
            
            if (limits.containsKey(name)) {
                entities = limits.get(name);
            }

            Vehicle vehicle = e.getVehicle();
            int total = entities.size();

            if (limit > 0 && total >= limit) {

                BCom.msg(p, Messages.limits_vehicles);
                vehicle.remove();

            } else {
                
                vehicle.setMetadata("CreativeVehicle", new FixedMetadataValue(EntryPoint.getPlugin(), "CreativeVehicle"));
                entities.add(vehicle.getUniqueId());
                
                limits.put(name, entities);
            }
        }
    }
    
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = false)
    public void onVehicleDestroy(VehicleDestroyEvent e) {
        
        Entity entity = e.getAttacker();
        Vehicle vehicle = e.getVehicle();
        
        if (!(entity instanceof Player)) {
            return;
        }
        
        if (_config == null) {
            _config = EntryPoint.get().getWorldConfig();
        }
        
        World w = entity.getWorld();
        
        WorldNodePack nodes = _config.get(w.getName());
        
        if (nodes.world_exclude) {
            return;
        }
        
        if (nodes.prevent_vehicle) {
            
            if (!vehicle.hasMetadata("CreativeVehicle")) {
                return;
            }
            
            e.setCancelled(true);
            vehicle.remove();
        }
    }
    
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = false)
    public void onEntityExplode(EntityExplodeEvent e) {
        
        if (e.isCancelled()) {
            return;
        }
        
        if (_config == null) {
            _config = EntryPoint.get().getWorldConfig();
        }

        World w = e.getLocation().getWorld();
        
        WorldNodePack nodes = _config.get(w.getName());
        
        if (nodes.world_exclude) {
            return;
        }
        
        if (nodes.block_explosion && (nodes.block_nodrop)) {
            
            List<Block> blocks = e.blockList();
            if (blocks == null || blocks.isEmpty()) {
                return;
            }
            
            if (_manager == null) {
                _manager = EntryPoint.get().getBlockManager();
            }
            
            if (_nodrop == null) {
                _nodrop = EntryPoint.get().getNoDrop();
            }
            
            Iterator<Block> it = blocks.iterator();
            
            while (it.hasNext()) {
                
                Block b = it.next();
                
                if (b.getType() != Material.AIR) {
                    if (nodes.block_nodrop) {
                        if (_manager.isProtected(b)){
                            _manager.unprotect(b);
                            _nodrop.removeDrop(b, null);
                        }
                    }
                }
            }
        }
    }
    
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = false)
    public void onEntityTarget(EntityTargetEvent e) {
       
        if (e.isCancelled()) {
            return;
        }
        
        Entity target = e.getTarget();
        if (!(target instanceof Player)) {
            return;
        }
        
        Player p = (Player) target;
        
        if (!p.getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        
        if (_config == null) {
            _config = EntryPoint.get().getWorldConfig();
        }
        
        World w = p.getWorld();
        WorldNodePack nodes = _config.get(w.getName());
        
        if (nodes.world_exclude) {
            return;
        }

        if (nodes.prevent_target) {
            
            if (_perm == null) {
                _perm = EntryPoint.get().getPermissions();
            }
            
            if (!_perm.hasPerm(p, "Preventions.Target")) {
                e.setCancelled(true);
            }
        }
    }
    
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = false)
    public void onPlayerInteractEntity(PlayerInteractEntityEvent e) {

        if (e.isCancelled()) {
            return;
        }

        Entity entity = e.getRightClicked();

        if (entity == null) {
            return;
        }
        
        Player p = e.getPlayer();
        
        if (!p.getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        
        if (_config == null) {
            _config = EntryPoint.get().getWorldConfig();
        }
        
        World w = p.getWorld();
        WorldNodePack nodes = _config.get(w.getName());
        
        if (nodes.world_exclude) {
            return;
        }
        
        if (_perm == null) {
            _perm = EntryPoint.get().getPermissions();
        }

        if (nodes.prevent_mcstore) {
            if ((entity instanceof StorageMinecart) || (entity instanceof PoweredMinecart)) {
                if (!_perm.hasPerm(p, "Preventions.MineCartStorage")) {
                    BCom.msg(p, Messages.mainode_restricted);
                    e.setCancelled(true); return;
                }
            }
        }

        if (nodes.prevent_villager) {
            if (entity instanceof Villager) {
                if (!_perm.hasPerm(p, "Preventions.InteractVillagers")) {
                    BCom.msg(p, Messages.mainode_restricted);
                    e.setCancelled(true); return;
                }
            }
        }

        if (nodes.prevent_frame) {
            if (entity instanceof ItemFrame) {
                if (!_perm.hasPerm(p, "Preventions.ItemFrame")) {
                    BCom.msg(p, Messages.mainode_restricted);
                    e.setCancelled(true);
                }
            }
        }
    }
    
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = false)
    public void onEntityDeath(EntityDeathEvent e) {
        
        Player p = e.getEntity().getKiller();
        
        if (p == null) {
            return;
        }
        
        if (!p.getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }

        if (_config == null) {
            _config = EntryPoint.get().getWorldConfig();
        }
        
        World w = p.getWorld();
        WorldNodePack nodes = _config.get(w.getName());
        
        if (nodes.world_exclude) {
            return;
        }
        
        if (nodes.prevent_mobsdrop) {
            if (e.getEntity() instanceof Creature) {
                
                if (_perm == null) {
                    _perm = EntryPoint.get().getPermissions();
                }
                
                if (!_perm.hasPerm(p, "Preventions.MobsDrop")) {
                    e.setDroppedExp(0);
                    e.getDrops().clear();
                }
            }
        }
    }
    
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = false)
    public void onEntityDamage(EntityDamageByEntityEvent e) {
        
        Entity entity = e.getDamager();
        
        World w = entity.getWorld();
        
        if (_config == null) {
            _config = EntryPoint.get().getWorldConfig();
        }
        
        WorldNodePack nodes = _config.get(w.getName());

        if (nodes.world_exclude) {
            return;
        }
        
        if (_perm == null) {
            _perm = EntryPoint.get().getPermissions();
        }

        if (((e.getDamager() instanceof Player)) && ((e.getEntity() instanceof Player))) { //Player versus Player

            Player attacker = (Player)e.getDamager();
            //Player defender = (Player)e.getEntity();

            if (nodes.prevent_pvp) {
                if (attacker.getGameMode().equals(GameMode.CREATIVE)) {
                    if (!_perm.hasPerm(attacker, "Preventions.PvP")) {
                        BCom.msg(attacker, Messages.mainode_restricted);
                        e.setCancelled(true);
                    }
                }
            }

        } else 
        if (((e.getDamager() instanceof Player)) && ((e.getEntity() instanceof Creature))) { //Player versus Creature
            
            Player attacker = (Player)e.getDamager();
            
            if (nodes.prevent_mobs) {
                if (attacker.getGameMode().equals(GameMode.CREATIVE)) {
                    if (!_perm.hasPerm(attacker, "Preventions.Mobs")) {
                        BCom.msg(attacker, Messages.mainode_restricted);
                        e.setCancelled(true);
                    }
                }
            }
            
        } else 
        if ((e.getDamager() instanceof Projectile)) { //Damage with projectiles [e.g Arrow, Eggs]
            Projectile projectile = (Projectile)e.getDamager();
            if (((projectile.getShooter() instanceof Player)) && ((e.getEntity() instanceof Player))) { //Player versus Player with projectiles
                Player attacker = (Player)projectile.getShooter();

                if (nodes.prevent_pvp) {
                    if (attacker.getGameMode().equals(GameMode.CREATIVE)) {
                        if (!_perm.hasPerm(attacker, "Preventions.PvP")) {
                            BCom.msg(attacker, Messages.mainode_restricted);
                            e.setCancelled(true);
                        }
                    }
                }

            } else 
            if (((projectile.getShooter() instanceof Player)) && ((e.getEntity() instanceof Creature))) { //Player versus Creature with projectiles
                Player attacker = (Player)projectile.getShooter();
                if (nodes.prevent_mobs) {
                    if (attacker.getGameMode().equals(GameMode.CREATIVE)) {
                        if (!_perm.hasPerm(attacker, "Preventions.Mobs")) {
                            BCom.msg(attacker, Messages.mainode_restricted);
                            e.setCancelled(true);
                        }
                    }
                }
            }
        }
    }
    
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = false)
    public void onExplosionPrime(ExplosionPrimeEvent e) {
        
        if (e.isCancelled()) {
            return;
        }
        
        Entity entity = e.getEntity();

        if (entity instanceof TNTPrimed) {
            
            TNTPrimed tnt = (TNTPrimed) entity;

            Block b = tnt.getLocation().getBlock();
            World w = b.getWorld();

            if (_config == null) {
                _config = EntryPoint.get().getWorldConfig();
            }
            
            WorldNodePack nodes = _config.get(w.getName());
            
            if (nodes.world_exclude) {
                return;
            }
            
            if (!nodes.misc_tnt) {
                return;
            }

            if (_manager == null) {
                _manager = EntryPoint.get().getBlockManager();
            }
            
            if (_manager.isProtected(b)) {
                
                b.setType(Material.TNT);
                e.setFire(false);
                e.setRadius(0);
                
                removeIgnition(b);

                e.setCancelled(true);
            }
        }
    }
    
    private void removeIgnition(Block b) {
        
        HashSet<Integer> blocks = new HashSet<Integer>(Arrays.asList(new Integer[]{10, 11, 27, 28, 51, 69, 70, 72, 73, 74, 75, 76, 77, 55, 331, 356}));

        if (_manager == null) {
            _manager = EntryPoint.get().getBlockManager();
        }
        
        /*
            ** @TODO: USE MATERIAL NAMES **
        */
            
        int x = b.getX(); int y = b.getY(); int z = b.getZ(); int radius = 2;
        
        int minX = x - radius; int minY = y - radius; int minZ = z - radius;
        int maxX = x + radius; int maxY = y + radius; int maxZ = z + radius;

        for (int counterX = minX; counterX < maxX; counterX++) {
            for (int counterY = minY; counterY < maxY; counterY++) {
                for (int counterZ = minZ; counterZ < maxZ; counterZ++) {
                    
                    Block block = b.getWorld().getBlockAt(counterX, counterY, counterZ);
                    
                    if (blocks.contains(block.getTypeId())) {
                        _manager.unprotect(block);
                        block.breakNaturally();
                    }
                }
            }
        }
    }
    
    public void waitFor(Player p) {
        this.waiting.add(p);
    }
    
    public void gc() throws Throwable {

        limits  .clear();
        waiting .clear();

        release();
    }
    
    public void release() {
        _config     = null;
        _manager    = null;
        _nodrop     = null;
        _perm       = null;
    }
    
    public void cleanup(Player p) {
        
        limits.remove(p.getName());
        Iterator<Player> it = waiting.iterator();
        
        while (it.hasNext()) {
            if (it.next().getName().equals(p.getName())) {
                it.remove();
            }
        }
    }
}