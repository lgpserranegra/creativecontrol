package me.FurH.CreativeControl.listeners;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import me.FurH.Bukkit.Core.player.BPlayerUtils;
import me.FurH.Bukkit.Core.util.BCom;
import me.FurH.Core.close.Closer;
import me.FurH.Core.database.SQLDb;
import me.FurH.Core.debug.Log;
import me.FurH.Core.list.CollectionUtils;
import me.FurH.Core.reference.ExpirableReference;
import me.FurH.CreativeControl.EntryPoint;
import me.FurH.CreativeControl.blacklist.BlackList;
import me.FurH.CreativeControl.config.MainNodePack;
import me.FurH.CreativeControl.config.world.WorldConfig;
import me.FurH.CreativeControl.config.world.WorldNodePack;
import me.FurH.CreativeControl.inventory.InventoryManager;
import me.FurH.CreativeControl.manager.BlockManager;
import me.FurH.CreativeControl.manager.PlayerManager;
import me.FurH.CreativeControl.messages.Messages;
import me.FurH.CreativeControl.permissions.CPerm;
import me.FurH.CreativeControl.region.RegionData;
import me.FurH.CreativeControl.region.RegionManager;
import me.FurH.CreativeControl.selection.AreaSelection;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.enchantment.EnchantItemEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryCreativeEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerEggThrowEvent;
import org.bukkit.event.player.PlayerGameModeChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

/**
 *
 * @author FurmigaHumana
 * All Rights Reserved unless otherwise explicitly stated.
 */
public class PlayerListener implements Listener {

    private final HashMap<String, ExpirableReference<Location>> point_right;
    private final HashMap<String, ExpirableReference<Location>> point_left;

    private BlackList _blacklist;
    private WorldConfig _config;
    private MainNodePack _main;

    private InventoryManager _inventory;
    private CPerm _perm;

    private RegionManager _regions;
    private SQLDb _database;
    private PlayerManager _players;
    private BlockManager _manager;
    
    public PlayerListener() {
        point_right = new HashMap<String, ExpirableReference<Location>>();
        point_left  = new HashMap<String, ExpirableReference<Location>>();
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerGameModeChange(PlayerGameModeChangeEvent e) {
        
        if (e.isCancelled()) {
            return;
        }
        
        Player p = e.getPlayer();

        GameMode newgm = e.getNewGameMode();
        GameMode oldgm = p.getGameMode();
        
        if (_main == null) {
            _main = EntryPoint.get().getMainNode();
        }

        if (_perm == null) {
            _perm = EntryPoint.get().getPermissions();
        }

        if (_main.data_inventory) {
            if (!_perm.hasPerm(p, "Data.Status")) {

                InventoryView view = p.getOpenInventory();
                view.close();

                if (_inventory == null) {
                    _inventory = EntryPoint.get().getInvManager();
                }

                try {

                    if (!_inventory.process(p, newgm, oldgm)) {
                        e.setCancelled(true); return;
                    }

                } catch (Throwable ex) {
                    ex.printStackTrace(); e.setCancelled(true); return;
                }
            }
        }
        
        if (_main.perm_enabled) {
            if (!_perm.hasPerm(p, "Permission.Change")) {

                Permission permissions = _perm.getVault();

                if (permissions != null) {

                    if (_database == null) {
                        _database = EntryPoint.get().getDb();
                    }

                    if (newgm.equals(GameMode.CREATIVE)) {

                        if (_main.perm_keep) {

                            permissions.playerAddGroup(p, _main.perm_creative);

                        } else {

                            String[] groups = permissions.getPlayerGroups(p);
                            Statement st = null;

                            try {

                                st = _database.statement(true);
                                st.execute("UPDATE `" + _main.database_prefix + "groups` SET groups = '" + Arrays.toString(groups) + "' WHERE player = '" + _players.playerId(p) + "';");

                            } catch (Throwable ex) {

                                BCom.msg(p, "§4Failed to change gamemode, groups error.");
                                Log.error(ex, "Failedto change gamemode group");

                                e.setCancelled(true); return;

                            } finally {
                                Closer.get().closeLater(st);
                            }

                            for (String group : groups) {
                                permissions.playerRemoveGroup(p, group);
                            }

                            permissions.playerAddGroup(p, _main.perm_creative);
                        }

                    } else {

                        if (_main.perm_keep) {

                            permissions.playerRemoveGroup(p, _main.perm_creative);

                        } else {

                            String[] current = permissions.getPlayerGroups(p);
                            String[] groups = null;

                            Statement st = null;
                            ResultSet rs = null;

                            try {

                                if (_players == null) {
                                    _players = EntryPoint.get().getPlayerManager();
                                }

                                st = _database.statement(false);
                                st.execute("SELECT groups FROM `" + _main.database_prefix + "groups` WHERE player = '" + _players.playerId(p) + "' LIMIT 1;");

                                rs = st.getResultSet();

                                if (rs.next()) {

                                    groups = CollectionUtils.toStringList(rs.getString("groups"), ", ").toArray(new String[1]);

                                } else {

                                    _database.statement(true).execute("INSERT INTO `" + _main.database_prefix + "groups` (player, groups) VALUES ('" + _players.playerId(p) + "', '');");
                                    _database.commit(false);
                                    
                                }

                            } catch (Throwable ex) {

                                BCom.msg(p, "§4Failed to change gamemode, groups error.");
                                Log.error(ex, "Failed to change gamemode");

                                e.setCancelled(true); return;
                            } finally {
                                Closer.get().closeLater(st, rs);
                            }

                            if (groups != null) {

                                Arrays.sort(groups, Collections.reverseOrder());

                                for (String group : current) {
                                    permissions.playerRemoveGroup(p, group);
                                }

                                for (String old : groups) {
                                    permissions.playerAddGroup(p, old);
                                }
                            }
                        }
                    }

                } else {
                    Log.log("The permissions function only works if Vault is installed!");
                }
            }
        }

        if (_regions == null) {
            _regions = EntryPoint.get().getRegionManager();
        }

        RegionData region = _regions.get(p.getLocation());

        if (region != null) {
            if (!newgm.equals(region.gamemode)) {

                if (_config == null) {
                    _config =  EntryPoint.get().getWorldConfig();
                }

                if (!newgm.equals(_config.get(p.getWorld().getName()).world_gamemode)) {
                    if (!_perm.hasPerm(p, "Region.Change")) {
                        BCom.msg(p, Messages.region_cant_change);
                        e.setCancelled(true); return;
                    }
                }
            }
        }
        
        if (!e.isCancelled() && newgm.equals(GameMode.CREATIVE)) {
            
            ItemStack helmet = _main.armor_helmet;
            ItemStack chestplate = _main.armor_chest;
            ItemStack leggings = _main.armor_leggs;
            ItemStack boots = _main.armor_boots;
            
            if (
                    helmet.getType() != Material.AIR ||
                    chestplate.getType() != Material.AIR ||
                    leggings.getType() != Material.AIR ||
                    boots.getType() != Material.AIR
                    ) {

                PlayerInventory inv = p.getInventory();

                inv.setHelmet(helmet);
                inv.setChestplate(chestplate);
                inv.setLeggings(leggings);
                inv.setBoots(boots);
            }
        }
    }
    
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = false)
    public void onPlayerCommand(PlayerCommandPreprocessEvent e) {

        if (e.isCancelled()) {
            return;
        }
        
        Player p = e.getPlayer();
        String world = p.getWorld().getName();
        
        if (_config == null) {
            _config = EntryPoint.get().getWorldConfig();
        }

        String cmd = e.getMessage().split(" ")[ 0 ].toLowerCase();
        WorldNodePack nodes = _config.get(world);
        
        if (nodes.world_exclude) {
            return;
        }
        
        if (_perm == null) {
            _perm = EntryPoint.get().getPermissions();
        }
            
        if (p.getGameMode().equals(GameMode.CREATIVE)) {

            if (nodes.black_cmds.contains(cmd)) {
                if (!_perm.hasPerm(p, "BlackList.Commands")) {
                    BCom.msg(p, Messages.blacklist_commands, p.getGameMode());
                    e.setCancelled(true);
                }
            }

        } else {

            if (nodes.black_s_cmds.contains(cmd)) {
                if (!_perm.hasPerm(p, "BlackList.SurvivalCommands")) {
                    BCom.msg(p, Messages.blacklist_commands, p.getGameMode());
                    e.setCancelled(true);
                }
            }
        }
    }
    
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = false)
    public void onPlayerDeath(PlayerDeathEvent e) {
        
        Player p = e.getEntity();
        
        if (!p.getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        
        if (_config == null) {
            _config = EntryPoint.get().getWorldConfig();
        }

        World w = p.getWorld();
        WorldNodePack nodes = _config.get(w.getName());

        if (nodes.world_exclude) {
            return;
        }

        if (nodes.prevent_drops) {
            
            if (_perm == null) {
                _perm = EntryPoint.get().getPermissions();
            }
            
            if (!_perm.hasPerm(p, "Preventions.ClearDrops")) {
                e.getDrops().clear();
                e.setDroppedExp(0);
            }
        }
    }
    
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = false)
    public void onEnchantItemEvent(EnchantItemEvent e) {

        if (e.isCancelled()) {
            return;
        }
        
        Player p = e.getEnchanter();
        
        if (!p.getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
                    
        if (_config == null) {
            _config = EntryPoint.get().getWorldConfig();
        }

        World w = p.getWorld();
        WorldNodePack nodes = _config.get(w.getName());
        
        if (nodes.world_exclude) {
            return;
        }

        if (nodes.prevent_enchant) {
            
            if (_perm == null) {
                _perm = EntryPoint.get().getPermissions();
            }
            
            if (!_perm.hasPerm(p, "Preventions.Enchantments")) {
                BCom.msg(p, Messages.mainode_restricted);
                e.setCancelled(true);
            }
        }
    }
    
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = false)
    public void onInventoryOpen(InventoryOpenEvent e) {
        
        if (e.isCancelled()) {
            return;
        }
        
        HumanEntity entity = e.getPlayer();
        if (!(entity instanceof Player)) {
            return;
        }
        
        Player p = (Player) entity;
        
        if (!p.getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        
        if (_config == null) {
            _config = EntryPoint.get().getWorldConfig();
        }

        World w = p.getWorld();
        WorldNodePack nodes = _config.get(w.getName());
        
        if (nodes.world_exclude) {
            return;
        }
        
        if (nodes.prevent_invinteract) {
            if (!_perm.hasPerm(p, "Preventions.InventoryOpen")) {
                BCom.msg(p, Messages.mainode_restricted);
                p.closeInventory();
                e.setCancelled(true);
            }
        }

        if (nodes.prevent_stacklimit > 0) { // @ TODO FIX
            for (ItemStack item : p.getInventory().getContents()) {
                if (item.getAmount() > nodes.prevent_stacklimit) {
                    item.setAmount(nodes.prevent_stacklimit);
                }
            }
        }
    }
    
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = false)
    public void onInventoryClick(InventoryCreativeEvent e) {

        if (e.isCancelled()) {
            return;
        }
        
        HumanEntity entity = e.getWhoClicked();
        
        if (!(entity instanceof Player)) {
            return;
        }
        
        Player p = (Player) entity;
        
        if (!p.getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        
        World w = p.getWorld();
        
        if (_config == null) {
            _config = EntryPoint.get().getWorldConfig();
        }
        
        WorldNodePack nodes = _config.get(w.getName());
        
        if (nodes.world_exclude) {
            return;
        }
        
        if (_perm == null) {
            _perm = EntryPoint.get().getPermissions();
        }
        
        if (nodes.prevent_invinteract) {
            if (e.getInventory().getType() == InventoryType.PLAYER) {
                if (e.getSlotType() != SlotType.QUICKBAR) {
                    if (e.getSlotType() == SlotType.ARMOR) {
                        
                        if (!_perm.hasPerm(p, "Preventions.InventoryArmor")) {
                            BCom.msg(p, Messages.mainode_restricted); return;
                        }
                        
                    } else {
                        
                        if (!_perm.hasPerm(p, "Preventions.InventoryInteract")) {
                            BCom.msg(p, Messages.mainode_restricted); return;
                        }
                    }
                }
            }
        }
        
        invItemCheck(p, e.getCurrentItem(), e, nodes);
        invItemCheck(p, e.getCursor(), e, nodes);
    }
    
    private void invItemCheck(Player p, ItemStack item, InventoryCreativeEvent e, WorldNodePack nodes) {

        if (nodes.world_exclude) {
            return;
        }
        
        if (p == null || item == null || e == null) {
            return;
        }
        
        if (!p.getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        
        if (_blacklist == null) {
            _blacklist = EntryPoint.get().getBlackList();
        }
        
        if (nodes.prevent_stacklimit > 0) {
            if (item.getAmount() > nodes.prevent_stacklimit) {
                if (!_perm.hasPerm(p, "Preventions.StackLimit")) {
                    item.setAmount(nodes.prevent_stacklimit);
                }
            }
        }
        
        Material type = item.getType();
        byte data = item.getData().getData();
        
        if (!_perm.hasPerm(p, "BlackList.Inventory")) {
            if (_blacklist.isBlackListed(type, data, nodes.black_inventory)) {
                e.setCancelled(true);
            } else
            if (_blacklist.isBlackListed(type, data, nodes.black_place)) {
                e.setCancelled(true);
            } else
            if (_blacklist.isBlackListed(type, data, nodes.black_use)) {
                e.setCancelled(true);
            }
        }
    }
    
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = false)
    public void onPlayerKick(PlayerKickEvent e) {
        cleanup(e.getPlayer());
    }
    
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = false)
    public void onPlayerQuit(PlayerQuitEvent e) {
        cleanup(e.getPlayer());
    }
    
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = false)
    public void onPlayerTeleport(PlayerTeleportEvent e) {
        // @ TODO
    }
    
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = false)
    public void onPlayerLogin(PlayerLoginEvent e) {
        // @ TODO
    }
    
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = false)
    public void onPlayerJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        EntryPoint.get().updatePlayer(p);
    }
    
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = false)
    public void onPlayerChangedWorld(PlayerChangedWorldEvent e) {
        
        Player  p = e.getPlayer();
        World   w = p.getWorld();
        
        if (_config == null) {
            _config = EntryPoint.get().getWorldConfig();
        }
        
        WorldNodePack nodes = _config.get(w.getName());
        
        if (nodes.world_changegm) {
            if (!p.getGameMode().equals(nodes.world_gamemode)) {
                
                if (_perm == null) {
                    _perm = EntryPoint.get().getPermissions();
                }

                if (!_perm.hasPerm(p, "World.Keep")) {

                    BCom.msg(p, Messages.region_unallowed, p.getGameMode().toString().toLowerCase());

                    if (nodes.world_gamemode != GameMode.CREATIVE) {
                        BPlayerUtils.toSafeLocation(p);
                    }

                    p.setGameMode(nodes.world_gamemode);
                }
            }
        }
    }
    
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = false)
    public void onPlayerPickupItem(PlayerPickupItemEvent e) {
        
        if (e.isCancelled()) {
            return;
        }
        
        Player p = e.getPlayer();
        
        if (!p.getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        
        if (_config == null) {
            _config = EntryPoint.get().getWorldConfig();
        }

        World w = p.getWorld();
        WorldNodePack nodes = _config.get(w.getName());
        
        if (nodes.world_exclude) {
            return;
        }
        
        if (nodes.prevent_pickup) {
            
            if (_perm == null) {
                _perm = EntryPoint.get().getPermissions();
            }

            if (!_perm.hasPerm(p, "Preventions.Pickup")) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = false)
    public void onPlayerDropItem(PlayerDropItemEvent e) {
        
        if (e.isCancelled()) {
            return;
        }
        
        Player p = e.getPlayer();
        
        if (!p.getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        
        if (_config == null) {
            _config = EntryPoint.get().getWorldConfig();
        }

        World w = p.getWorld();
        WorldNodePack nodes = _config.get(w.getName());
        
        if (nodes.prevent_drop) {
            
            if (_perm == null) {
                _perm = EntryPoint.get().getPermissions();
            }
            
            if (!_perm.hasPerm(p, "Preventions.ItemDrop")) {
                BCom.msg(p, Messages.mainode_restricted);
                e.setCancelled(true);
            }
        }
    }
    
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = false)
    public void onPlayerEggThrowEvent(PlayerEggThrowEvent e) {
        
        Player p = e.getPlayer();
        
        if (!p.getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        
        if (_config == null) {
            _config = EntryPoint.get().getWorldConfig();
        }

        World w = p.getWorld();
        WorldNodePack nodes = _config.get(w.getName());
        
        if (nodes.prevent_eggs) {
            
            if (_perm == null) {
                _perm = EntryPoint.get().getPermissions();
            }
            
            if (!_perm.hasPerm(p, "Preventions.Eggs")) {
                BCom.msg(p, Messages.mainode_restricted);
                e.setHatching(false);
                e.setNumHatches((byte) 0);
            }
        }
    }
    
    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = false)
    public void onPlayerInteract(PlayerInteractEvent e) {
        
        if (e.isCancelled()) {
            return;
        }

        final Player p = e.getPlayer();
        Block clicked = e.getClickedBlock();

        World w = clicked.getWorld();
        
        if (_config == null) {
            _config = EntryPoint.get().getWorldConfig();
        }
        
        WorldNodePack nodes = _config.get(w.getName());
        Action action = e.getAction();
        
        if (_main == null) {
            _main = EntryPoint.get().getMainNode();
        }
        
        if (_perm == null) {
            _perm = EntryPoint.get().getPermissions();
        }

        if (_main.selection_tool == p.getItemInHand().getType()) {
            
            if (_perm.hasPerm(p, "Utily.Selection")) {
                
                if (action == Action.RIGHT_CLICK_BLOCK) {

                    Location right = e.getClickedBlock().getLocation();

                    this.point_right.put(p.getName(), new ExpirableReference<Location>(right, 3, TimeUnit.MINUTES, new Runnable() {
                        @Override
                        public void run() {
                            point_right.remove(p.getName());
                        }
                    }));

                    BCom.msg(p, Messages.selection_first, right.getBlockX(), right.getBlockY(), right.getBlockZ());

                    e.setCancelled(true);

                } else
                if (action == Action.LEFT_CLICK_BLOCK) {

                    Location left = e.getClickedBlock().getLocation();

                    this.point_left.put(p.getName(), new ExpirableReference<Location>(left, 3, TimeUnit.MINUTES, new Runnable() {
                        @Override
                        public void run() {
                            point_left.remove(p.getName());
                        }
                    }));

                    BCom.msg(p, Messages.selection_second, left.getBlockX(), left.getBlockY(), left.getBlockZ());

                    e.setCancelled(true);
                }
            }
        }
        
        if (nodes.world_exclude) {
            return;
        }
        
        GameMode gm = p.getGameMode();
        
        if (_blacklist == null) {
            _blacklist = EntryPoint.get().getBlackList();
        }
        
        if (action == Action.RIGHT_CLICK_BLOCK) {
            if (gm.equals(GameMode.CREATIVE)) {
                
                Block b = e.getClickedBlock();
                
                if (_blacklist.isBlackListed(b.getType(), b.getData(), nodes.black_interact)) {
                    if (!_perm.hasPerm(p, "BlackList.ItemInteract")) {
                        BCom.msg(p, Messages.mainode_restricted);
                        e.setCancelled(true);
                        return;
                    }
                }
            }
        }
        
        Material mat = e.getMaterial();
        
        if ((
                action == Action.LEFT_CLICK_BLOCK || 
                action == Action.RIGHT_CLICK_BLOCK) && 
                (
                    mat == Material.MINECART || 
                    mat == Material.BOAT || 
                    mat == Material.STORAGE_MINECART || 
                    mat == Material.POWERED_MINECART || 
                    mat == Material.EXPLOSIVE_MINECART || 
                    mat == Material.HOPPER_MINECART)
                ) {

            if (gm.equals(GameMode.CREATIVE)) {
                EntryPoint.get().getEntityListener().waitFor(p);
                
            }
        }
        
        if ((action == Action.RIGHT_CLICK_AIR) || (action == Action.RIGHT_CLICK_BLOCK)) {
            if (gm.equals(GameMode.CREATIVE)) {
                
                ItemStack eit = e.getItem();
                
                if (eit != null) {
                    if (_blacklist.isBlackListed(eit.getType(), eit.getData().getData(), nodes.black_use)) {
                        if (!_perm.hasPerm(p, "BlackList.ItemUse")) {
                            BCom.msg(p, Messages.mainode_restricted);
                            e.setCancelled(true); return;
                        }
                    }
                }
                
                ItemStack phand = p.getItemInHand();
                
                if (phand != null) {

                    Material type = phand.getType();
                    
                    if (_blacklist.isBlackListed(type, phand.getData().getData(), nodes.black_use)) {
                        if (!_perm.hasPerm(p, "BlackList.ItemUse")) {
                            BCom.msg(p, Messages.mainode_restricted);
                            e.setCancelled(true); return;
                        }
                    }
                    
                    if (nodes.prevent_eggs) {
                        if ((type == Material.MONSTER_EGG) || (type == Material.MONSTER_EGGS)) {
                            if (!_perm.hasPerm(p, "Preventions.Eggs")) {
                                BCom.msg(p, Messages.mainode_restricted);
                                e.setCancelled(true); return;
                            }
                        }
                    }
                    
                    if (nodes.prevent_potion) {
                        if (type == Material.POTION) {
                            if (!_perm.hasPerm(p, "Preventions.PotionSplash")) {
                                BCom.msg(p, Messages.mainode_restricted);
                                e.setCancelled(true); return;
                            }
                        }
                    }
                }
            }
        }
    }
    
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = false)
    public void onBucketFill(PlayerBucketFillEvent e) {
        
        if (e.isCancelled()) {
            return;
        }
        
        //Player p = e.getPlayer();
        Block b = e.getBlockClicked();
        World w = b.getWorld();

        if (_config == null) {
            _config = EntryPoint.get().getWorldConfig();
        }
        
        WorldNodePack nodes = _config.get(w.getName());
        
        if (nodes.world_exclude) {
            return;
        }
        
        if (nodes.misc_liquid) {
            
            if (_manager == null) {
                _manager = EntryPoint.get().getBlockManager();
            }

            if (nodes.block_nodrop) {
                _manager.unprotect(b);
            }
        }
    }
    
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = false)
    public void onBucketEmpty(PlayerBucketEmptyEvent e) {
        
        if (e.isCancelled()) {
            return;
        }
        
        Player p = e.getPlayer();
        World w = p.getWorld();

        if (_config == null) {
            _config = EntryPoint.get().getWorldConfig();
        }

        WorldNodePack nodes = _config.get(w.getName());
        
        if (nodes.world_exclude) {
            return;
        }
        
        Material bucket = e.getBucket();
        
        Block bDown = e.getBlockClicked();
        Block b = bDown.getRelative(e.getBlockFace());
        
        if (p.getGameMode().equals(GameMode.CREATIVE)) {
            if (nodes.misc_liquid) {
                
                if ((bucket == Material.WATER_BUCKET) || (bucket == Material.LAVA_BUCKET) || (bucket == Material.BUCKET) || (bucket == Material.MILK_BUCKET)) {
                    
                    if (_manager == null) {
                        _manager = EntryPoint.get().getBlockManager();
                    }
                    
                    if (nodes.block_nodrop) {

                        if (bucket == Material.WATER_BUCKET) {
                            b.setType(Material.STATIONARY_WATER);
                        } else
                        if (bucket == Material.LAVA_BUCKET) {
                            b.setType(Material.STATIONARY_LAVA);
                        }
                        
                        _manager.unprotect(b);
                    }
                }
            }
        }
    }
    
    public AreaSelection getSelection(String pname) throws Exception {
        
        if (!point_left.containsKey(pname) || !point_right.containsKey(pname)) {
            return null;
        }
        
        ExpirableReference<Location> loc1 = point_right.get(pname);
        ExpirableReference<Location> loc2 = point_left.get(pname);
        
        if (loc1.get() == null || loc2.get() == null) {
            return null;
        }

        return new AreaSelection(loc1.get(), loc2.get());
    }
    
    public void gc() throws Throwable {

        point_right .clear();
        point_left  .clear();

        release();
    }
    
    public void release() {
        
        _blacklist = null;
        _config = null;
        _main = null;

        _inventory = null;
        _perm = null;

        _regions = null;
        _database = null;
        _players = null;
    }

    private void cleanup(Player p) {
        
        point_right .remove(p.getName());
        point_left  .remove(p.getName());
        
        EntryPoint.get().cleanup(p);
    }
}