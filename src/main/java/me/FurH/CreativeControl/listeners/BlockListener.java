package me.FurH.CreativeControl.listeners;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import me.FurH.Bukkit.Core.blocks.BBlockUtils;
import me.FurH.Bukkit.Core.util.BCom;
import me.FurH.Core.debug.Log;
import me.FurH.CreativeControl.EntryPoint;
import me.FurH.CreativeControl.blacklist.BlackList;
import me.FurH.CreativeControl.config.world.WorldConfig;
import me.FurH.CreativeControl.config.world.WorldNodePack;
import me.FurH.CreativeControl.manager.BlockLimit;
import me.FurH.CreativeControl.manager.BlockManager;
import me.FurH.CreativeControl.messages.Messages;
import me.FurH.CreativeControl.nodrop.BlockNoDrop;
import me.FurH.CreativeControl.permissions.CPerm;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockFadeEvent;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.material.MaterialData;
import org.bukkit.material.PistonBaseMaterial;

/**
 *
 * @author FurmigaHumana
 * All Rights Reserved unless otherwise explicitly stated.
 */
public class BlockListener implements Listener {

    private HashMap<String, BlockLimit> limits;
    
    private BlackList       _blacklist;
    private WorldConfig     _config;
    private BlockManager    _manager;
    private BlockNoDrop     _nodrop;
    private CPerm           _perm;
    
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onBlockPlace(BlockPlaceEvent e) {

        if (e.isCancelled()) {
            return;
        }
                
        Player  p = e.getPlayer();
        
        if (p == null) {
            return; // weird how this happens, right?
        }

        Block   b = e.getBlockPlaced();
        World   w = b.getWorld();
        
        if (_config == null) {
            _config = EntryPoint.get().getWorldConfig();
        }

        WorldNodePack nodes = _config.get(w.getName());

        if (nodes.world_exclude) {
            return;
        }
        
        GameMode gm = p.getGameMode();

        if (_perm == null) {
            _perm = EntryPoint.get().getPermissions();
        }
        
        if (gm.equals(GameMode.CREATIVE)) {

            // @ TODO: b.getData() is deprecated, figure this out.

            if (_blacklist == null) {
                _blacklist = EntryPoint.get().getBlackList();
            }

            if (_blacklist.isBlackListed(b.getType(), b.getData(), nodes.black_place)) {
                if (!_perm.hasPerm(p, "BlackList.BlockPlace")) {
                    BCom.msg(p, Messages.mainode_restricted);
                    e.setCancelled(true); return;
                }
            }

            if (nodes.prevent_wither) {

                if (b.getType() == Material.SKULL && !_perm.hasPerm(p, "Preventions.Wither")) {

                    if ((w.getBlockAt(b.getX(), b.getY() - 1, b.getZ()).getType() == Material.SOUL_SAND) &&
                            (w.getBlockAt(b.getX(), b.getY() - 2, b.getZ()).getType() == Material.SOUL_SAND) &&
                            (w.getBlockAt(b.getX() + 1, b.getY() - 1, b.getZ()).getType() == Material.SOUL_SAND) &&
                            (w.getBlockAt(b.getX() - 1, b.getY() - 1, b.getZ()).getType() == Material.SOUL_SAND) ||
                            (w.getBlockAt(b.getX(), b.getY() - 1, b.getZ() - 1).getType() == Material.SOUL_SAND) &&
                            (w.getBlockAt(b.getX(), b.getY() - 1, b.getZ() + 1).getType() == Material.SOUL_SAND)) {

                        BCom.msg(p, Messages.mainode_restricted);
                        e.setCancelled(true); return;
                    }
                }
            }
            
            if (nodes.prevent_snowgolem) {
                if (!_perm.hasPerm(p, "Preventions.SnowGolem")) {
                    if (((b.getType() == Material.PUMPKIN) || (b.getType() == Material.JACK_O_LANTERN)) &&
                            (w.getBlockAt(b.getX(), b.getY() - 1, b.getZ()).getType() == Material.SNOW_BLOCK) &&
                            (w.getBlockAt(b.getX(), b.getY() - 2, b.getZ()).getType() == Material.SNOW_BLOCK)) {

                        BCom.msg(p, Messages.mainode_restricted);
                        e.setCancelled(true); return;
                    }
                }
            }
            
            if (nodes.prevent_irongolem) {

                if (!_perm.hasPerm(p, "Preventions.IronGolem")) {
                    if (((b.getType() == Material.PUMPKIN) || (b.getType() == Material.JACK_O_LANTERN)) && 
                            (w.getBlockAt(b.getX(), b.getY() - 1, b.getZ()).getType() == Material.IRON_BLOCK) &&
                            (w.getBlockAt(b.getX(), b.getY() - 2, b.getZ()).getType() == Material.IRON_BLOCK) &&
                            (((w.getBlockAt(b.getX() + 1, b.getY() - 1, b.getZ()).getType() == Material.IRON_BLOCK) &&
                            (w.getBlockAt(b.getX() - 1, b.getY() - 1, b.getZ()).getType() == Material.IRON_BLOCK)) ||
                            ((w.getBlockAt(b.getX(), b.getY() - 1, b.getZ() + 1).getType() == Material.IRON_BLOCK) &&
                            (w.getBlockAt(b.getX(), b.getY() - 1, b.getZ() - 1).getType() == Material.IRON_BLOCK)))) {

                        BCom.msg(p, Messages.mainode_restricted);
                        e.setCancelled(true); return;
                    }
                }
            }
            
            /* piston fix */
            if (nodes.block_pistons) {
                
                BlockFace[] faces = new BlockFace[] { BlockFace.UP, BlockFace.DOWN, BlockFace.NORTH, BlockFace.SOUTH, BlockFace.EAST, BlockFace.WEST };
                
                for (BlockFace face : faces) {

                    Block relative = b.getRelative(face);

                    if (relative.getType() == Material.PISTON_BASE || relative.getType() == Material.PISTON_EXTENSION || relative.getType() == Material.PISTON_MOVING_PIECE || relative.getType() == Material.PISTON_STICKY_BASE) {

                        int data = relative.getData();

                        BlockFace head =
                                (data == 0 ? BlockFace.UP : (data == 1 ? BlockFace.DOWN : (data == 2 ? BlockFace.SOUTH : (data == 3 ? BlockFace.NORTH : (data == 4 ? BlockFace.EAST : BlockFace.WEST)))));

                        Block front = relative.getRelative(head.getOppositeFace());
                        
                        if (front.getLocation().equals(b.getLocation())) {
                            e.setCancelled(true); return;
                        }

                        break;
                    }
                }
            }
            /* piston fix */
        }
        
        int block_limit = nodes.block_minutelimit;

        if (block_limit > 0) {
            if (isLimitReached(p, block_limit)) {
                BCom.msg(p, Messages.blockmanager_limit);
                e.setCancelled(true); return;
            }
        }

        if (nodes.block_nodrop) {

            if (_manager == null) {
                _manager = EntryPoint.get().getBlockManager();
            }
            
            if (nodes.block_nodrop) {

                if (nodes.misc_liquid) {
                    Block r = e.getBlockReplacedState().getBlock();
                    if (r.getType() != Material.AIR) {
                        _manager.unprotect(r);
                    }
                }

                if (gm.equals(GameMode.CREATIVE)) {
                    if (!_perm.hasPerm(p, "NoDrop.DontSave")) {
                        if (!_manager.protect(p, b)) {
                            Log.log("Something went wrong...");
                            e.setCancelled(true); return;
                        }
                    }
                }

                return;
            }
        }
    }
    
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onBlockBreak(BlockBreakEvent e) {
                   
        if (e.isCancelled()) {
            return;
        }

        Player  p = e.getPlayer();
        
        if (p == null) {
            return;
        }
        
        Block   b = e.getBlock();
        World   w = e.getBlock().getWorld();
        
        if (_config == null) {
            _config = EntryPoint.get().getWorldConfig();
        }

        WorldNodePack nodes = _config.get(w.getName());
        
        if (nodes.world_exclude) {
            return;
        }
        
        if (_perm == null) {
            _perm = EntryPoint.get().getPermissions();
        }
        
        GameMode gm = p.getGameMode();
        
        if (gm.equals(GameMode.CREATIVE)) {
            
            if (nodes.prevent_bedrock) {
                if (b.getType() == Material.BEDROCK) {
                    if (b.getY() < 1) {
                        if (!_perm.hasPerm(p, "Preventions.BreakBedRock")) {
                            BCom.msg(p, Messages.mainode_restricted);
                            e.setCancelled(true); return;
                        }
                    }
                }
            }

            if (_blacklist == null) {
                _blacklist = EntryPoint.get().getBlackList();
            }

            if (_blacklist.isBlackListed(b.getType(), b.getData(), nodes.black_break)) {
                if (!_perm.hasPerm(p, "BlackList.BlockBreak")) {
                    BCom.msg(p, Messages.mainode_restricted);
                    e.setCancelled(true); return;
                }
            }
        }
        
        List<Block> attached = new ArrayList<Block>();

        if (nodes.block_nodrop) {

            if (_manager == null) {
                _manager = EntryPoint.get().getBlockManager();
            }

            if (nodes.block_attach) {
                
                // @ TODO: CHECK FOR PHYSICS

                BBlockUtils.getAttachedBlock(attached, b);
            }
            
            attached.add(b);

            if (nodes.block_nodrop) {

                for (Block at : attached) {
                    if (_manager.isProtected(at)) {
                        process(nodes, e, at, p);
                    }
                }

                return;
            }
        }
    }
    
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = false)
    public void onPistonExtend(BlockPistonExtendEvent e) {
        
        if (e.isCancelled()) {
            return;
        }

        if (_config == null) {
            _config = EntryPoint.get().getWorldConfig();
        }
        
        Block b = e.getBlock();
        
        if (b == null || b.getType() == Material.AIR) {
            // @ TODO: GLITCH CHECK
            return;
        }
        
        World w = b.getWorld();

        WorldNodePack nodes = _config.get(w.getName());
        
        if (nodes.world_exclude) {
            return;
        }
        
        if (nodes.block_pistons) {
            
            List<Block> bs = e.getBlocks();

            if (bs.isEmpty()) {
                return;
            }

            if (_manager == null) {
                _manager = EntryPoint.get().getBlockManager();
            }
            
            for (Block block : bs) {
                
                if (block.getType() == Material.AIR) {
                    continue;
                }
                
                if (_manager.isProtected(block)) {
                    e.setCancelled(true); break;
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = false)
    public void onPistonRetract(BlockPistonRetractEvent e) {
        
        if (e.isCancelled()) {
            return;
        }
        
        if (!e.isSticky()) {
            return;
        }
        
        Block b = e.getBlock();
        
        if (b == null || b.getType() == Material.AIR) {
            return;
        }
        
        if (_config == null) {
            _config = EntryPoint.get().getWorldConfig();
        }

        World w = b.getWorld();

        WorldNodePack nodes = _config.get(w.getName());
        
        if (nodes.world_exclude) {
            return;
        }
        
        if (nodes.block_pistons) {
            
            MaterialData data = b.getState().getData();
            
            if (!(data instanceof PistonBaseMaterial)) {
                return;
            }

            BlockFace direction = ((PistonBaseMaterial) data).getFacing();
            Block moved = b.getRelative(direction, 2);

            if (_manager == null) {
                _manager = EntryPoint.get().getBlockManager();
            }

            if (_manager.isProtected(moved)) {
                e.setCancelled(true);
            }
        }
    }
    
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = false)
    public void onEntityChangeBlock(EntityChangeBlockEvent e) {
        
        if (e.isCancelled()) {
            return;
        }
        
        Entity entity = e.getEntity();
        
        if (entity instanceof FallingBlock) {
            
            Block b = e.getBlock();
            
            if (b == null || b.getType() == Material.AIR) {
                return;
            }
            
            World w = b.getWorld();

            if (_config == null) {
                _config = EntryPoint.get().getWorldConfig();
            }
            
            WorldNodePack nodes = _config.get(w.getName());
            
            if (nodes.world_exclude) {
                return;
            }
            
            if (!nodes.block_physics) {
                return;
            }
            
            if (_manager == null) {
                _manager = EntryPoint.get().getBlockManager();
            }

            if (_manager.isProtected(b)) {
                e.setCancelled(true);
            }
        }
    }
    
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = false)
    public void onBlockFromTo(BlockFromToEvent e) {
        
        if (e.isCancelled()) {
            return;
        }
        
        Block b = e.getToBlock();
        Material type = b.getType();

        if (type != Material.WATER
                && type != Material.STATIONARY_WATER
                && type != Material.LAVA
                && type != Material.STATIONARY_LAVA) {
            return;
        }

        World w = b.getWorld();

        if (_config == null) {
            _config = EntryPoint.get().getWorldConfig();
        }
        
        WorldNodePack nodes = _config.get(w.getName());
        
        if (nodes.world_exclude) {
            return;
        }
        
        if (nodes.misc_liquid) {
            
            if (type == Material.WATER || type == Material.STATIONARY_WATER) {
                b.setType(Material.STATIONARY_WATER);
                e.setCancelled(true); return;
            }
            
            if (type == Material.LAVA || type == Material.STATIONARY_LAVA) {
                b.setType(Material.STATIONARY_LAVA);
                e.setCancelled(true); return;
            }
        }
        
        if (nodes.block_water) {

            if (nodes.block_nodrop) {

                if (nodes.block_nodrop) {

                    if (_manager == null) {
                        _manager = EntryPoint.get().getBlockManager();
                    }

                    if (_manager.isProtected(b)) {
                        _manager.unprotect(b);

                        if (_nodrop == null) {
                            _nodrop = EntryPoint.get().getNoDrop();
                        }

                        _nodrop.removeDrop(b, null);
                    }
                }
            }
        }
    }
    
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = false)
    public void onBucketEmpty(PlayerBucketEmptyEvent e) {
        
        if (e.isCancelled()) {
            return;
        }
        
        Block clicked = e.getBlockClicked();

        if (clicked == null) {
            return;
        }
        
        World w = clicked.getWorld();
        
        if (_config == null) {
            _config = EntryPoint.get().getWorldConfig();
        }
        
        WorldNodePack nodes = _config.get(w.getName());
        
        if (nodes.world_exclude) {
            return;
        }
        
        if (!nodes.block_water) {
            return;
        }

        Block b = clicked.getRelative(BlockFace.UP);
        int id = b.getType().getId();

        if (!isWaterAffected(id)) {
            return;
        }
        
        if (nodes.block_nodrop) {

            if (nodes.block_nodrop) {
                
                if (_manager == null) {
                    _manager = EntryPoint.get().getBlockManager();
                }

                if (_manager.isProtected(b)) {
                    _manager.unprotect(b);

                    if (_nodrop == null) {
                        _nodrop = EntryPoint.get().getNoDrop();
                    }
                    
                    _nodrop.removeDrop(b, null);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = false)
    public void onBlockIgnite(BlockIgniteEvent e) {
        
        if (e.isCancelled()) {
            return;
        }
        
        Block b = e.getBlock();
        
        if (_config == null) {
            _config = EntryPoint.get().getWorldConfig();
        }
        
        World w = b.getWorld();
        WorldNodePack nodes = _config.get(w.getName());
        
        if (nodes.world_exclude) {
            return;
        }
        
        if (!nodes.misc_fire) {
            return;
        }

        if (_manager == null) {
            _manager = EntryPoint.get().getBlockManager();
        }

        if (_manager.isProtected(b)) {
            e.setCancelled(true);
        }
    }
    
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = false)
    public void onBlockBurn(BlockBurnEvent e) {

        if (e.isCancelled()) {
            return;
        }

        Block b = e.getBlock();
        
        if (_config == null) {
            _config = EntryPoint.get().getWorldConfig();
        }
        
        World w = b.getWorld();
        WorldNodePack nodes = _config.get(w.getName());
        
        if (nodes.world_exclude) {
            return;
        }
        
        if (!nodes.misc_fire) {
            return;
        }

        if (_manager == null) {
            _manager = EntryPoint.get().getBlockManager();
        }

        if (_manager.isProtected(b)) {

            removeFire(b);
            e.setCancelled(true);
        }
    }
    
    private void removeFire(Block b) {
        
        int x = b.getX(); int y = b.getY(); int z = b.getZ(); int radius = 5;
        
        int minX = x - radius; int minY = y - radius; int minZ = z - radius;
        int maxX = x + radius; int maxY = y + radius; int maxZ = z + radius;

        for (int counterX = minX; counterX < maxX; counterX++) {
            for (int counterY = minY; counterY < maxY; counterY++) {
                for (int counterZ = minZ; counterZ < maxZ; counterZ++) {

                    Block block = b.getWorld().getBlockAt(counterX, counterY, counterZ);

                    if (block.getType() == Material.FIRE) {
                        block.setType(Material.AIR);
                    }
                }
            }
        }
    }
    
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = false)
    public void onBlockFade(BlockFadeEvent e) {
        
        if (e.isCancelled()) {
            return;
        }
        
        Block b = e.getBlock();

        if (_config == null) {
            _config = EntryPoint.get().getWorldConfig();
        }
        
        World w = b.getWorld();
        WorldNodePack nodes = _config.get(w.getName());
        
        if (nodes.world_exclude) {
            return;
        }
        
        if (!nodes.misc_ice) {
            return;
        }

        if (_manager == null) {
            _manager = EntryPoint.get().getBlockManager();
        }

        if (_manager.isProtected(b)) {
            e.setCancelled(true);
        }
    }

    private boolean isWaterAffected(int id) {
        return id == 6 
                || id == 30
                || id == 31
                || id == 37
                || id == 38
                || id == 39
                || id == 40
                || id == 50
                || id == 78
                || id == 390
                || id == 397
                || id == 69
                || id == 75
                || id == 76
                || id == 77
                || id == 131
                || id == 143
                || id == 55
                || id == 404
                || id == 356
                || id == 27
                || id == 28
                || id == 66
                || id == 157;
    }

    private boolean isLimitReached(Player player, int limit) {
        
        if (limits == null) {
            limits = new HashMap<String, BlockLimit>();
        }
        
        if (_perm.hasPerm(player, "Preventions.MinuteLimit")) {
            return false;
        }

        String key = player.getName() + "_" + player.getWorld().getName();
        
        BlockLimit data = limits.get(key);

        if (data == null) {
            data = new BlockLimit();
            limits.put(key, data);
        }

        if (data.expired()) {
            data.reset();
        }

        data.increment();

        return data.placed() > limit;
    }

    private void process(WorldNodePack nodes, BlockBreakEvent e, Block b, Player p) {

        if (!e.isCancelled()) {
            
            if (nodes.block_creative) {
                if (!p.getGameMode().equals(GameMode.CREATIVE)) {
                    BCom.msg(p, Messages.blockbreak_cantbreak);
                    e.setCancelled(true); return;
                }
            }
            
            // we wont check if the manager is null
            _manager.unprotect(b);
                        
            e.setExpToDrop(0);
            
            if (!b.getDrops().isEmpty()) {

                if (_nodrop == null) {
                    _nodrop = EntryPoint.get().getNoDrop();
                }

                _nodrop.removeDrop(b, p.getItemInHand());
            }
            
            if (!p.getGameMode().equals(GameMode.CREATIVE)) {
                BCom.msg(p, Messages.blockbreak_creativeblock);
            }
        }
    }
    
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = false)
    public void onItemSpawn(ItemSpawnEvent e) {

        if (e.isCancelled()) {
            return;
        }
        
        Item item = e.getEntity();
        
        if (item != null) {

            Location loc = item.getLocation().getBlock().getLocation();
            
            if (_nodrop == null) {
                _nodrop = EntryPoint.get().getNoDrop();
            }

            if (_nodrop.isDropRemoved(loc, item)) {
                e.setCancelled(true);
            }
        }
    }

    public void gc() throws Throwable {

        if (limits != null) {
            limits.clear();
        }

        release();
    }

    public void release() {
        _blacklist  = null;
        _config     = null;
        _manager    = null;
        _nodrop     = null;
        _perm       = null;
    }
    
    public void cleanup(Player p) {
        
        if (limits == null) {
            return;
        }
        
        Iterator<String> it = limits.keySet().iterator();
        
        while (it.hasNext()) {
            if (it.next().toLowerCase().startsWith(p.getName().toLowerCase())) {
                it.remove();
            }
        }
    }
}