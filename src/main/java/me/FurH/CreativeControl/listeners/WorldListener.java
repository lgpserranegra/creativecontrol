package me.FurH.CreativeControl.listeners;

import me.FurH.Bukkit.Core.util.BCom;
import me.FurH.CreativeControl.EntryPoint;
import me.FurH.CreativeControl.config.world.WorldConfig;
import me.FurH.CreativeControl.config.world.WorldNodePack;
import me.FurH.CreativeControl.messages.Messages;
import me.FurH.CreativeControl.permissions.CPerm;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.world.StructureGrowEvent;
import org.bukkit.event.world.WorldLoadEvent;
import org.bukkit.event.world.WorldUnloadEvent;

/**
 *
 * @author FurmigaHumana
 * All Rights Reserved unless otherwise explicitly stated.
 */
public class WorldListener implements Listener {
    
    private WorldConfig   _config;
    private CPerm         _perm;

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = false)
    public void onWorldLoad(WorldLoadEvent e) {
        EntryPoint.get().loadWorld(e.getWorld());
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = false)
    public void onWorldUnload(WorldUnloadEvent e) {
        EntryPoint.get().unload(e.getWorld());
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = false)
    public void onStructureGrown(StructureGrowEvent e) {
        
        if (e.isCancelled()) {
            return;
        }
        
        if (!e.isFromBonemeal()) {
            return;
        }
        
        Player p = e.getPlayer();
        
        if (p == null) {
            return;
        }
        
        if (!p.getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        
        if (_config == null) {
            _config = EntryPoint.get().getWorldConfig();
        }
        
        WorldNodePack nodes = _config.get(p.getWorld().getName());
        
        if (nodes.world_exclude) {
            return;
        }
        
        if (_perm != null) {
            _perm = EntryPoint.get().getPermissions();
        }

        if (!_perm.hasPerm(p, "Preventions.Bonemeal")) {
            BCom.msg(p, Messages.mainode_restricted);
            e.setCancelled(true);
        }
    }

    public void gc() {
        release(); // just in case we need it in the future
    }

    public void release() {
        _config = null;
        _perm   = null;
    }
}
