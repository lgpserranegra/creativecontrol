package me.FurH.CreativeControl.listeners;

import me.FurH.Bukkit.Core.player.BPlayerUtils;
import me.FurH.Bukkit.Core.util.BCom;
import me.FurH.CreativeControl.EntryPoint;
import me.FurH.CreativeControl.config.world.WorldConfig;
import me.FurH.CreativeControl.config.world.WorldNodePack;
import me.FurH.CreativeControl.messages.Messages;
import me.FurH.CreativeControl.permissions.CPerm;
import me.FurH.CreativeControl.region.RegionData;
import me.FurH.CreativeControl.region.RegionManager;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 *
 * @author FurmigaHumana
 * All Rights Reserved unless otherwise explicitly stated.
 */
public class MoveListener implements Listener {
    
    private WorldConfig     _config;
    private RegionManager   _regions;
    private CPerm           _perm;
    
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = false)
    public void PlayerMoveEvent(PlayerMoveEvent e) {
        
        if (e.isCancelled()) {
            return;
        }

        Location from = e.getFrom();
        Location to = e.getTo();

        if (to.getBlockX() == from.getBlockX() &&
            to.getBlockY() == from.getBlockY() &&
            to.getBlockZ() == from.getBlockZ()) {
            
            return;
        }
        
        Player p = e.getPlayer();
        World w = p.getWorld();
        
        if (_config == null) {
            _config = EntryPoint.get().getWorldConfig();
        }
        
        WorldNodePack nodes = _config.get(w.getName());
        
        if (nodes.world_exclude) {
            return;
        }
        
        if (_regions == null) {
            _regions = EntryPoint.get().getRegionManager();
        }
        
        RegionData region = _regions.get(to);
        
        if (_perm == null) {
            _perm = EntryPoint.get().getPermissions();
        }
        
        GameMode gm = p.getGameMode();

        if (region != null) {

            GameMode type = region.gamemode;

            if (!gm.equals(type)) {
                
                if (!_perm.hasPerm(p, "Region.Keep")) {

                    BCom.msg(p, Messages.region_welcome, type.toString().toLowerCase());

                    if (type != GameMode.CREATIVE) {
                        BPlayerUtils.toSafeLocation(p);
                    }
                    
                    p.setGameMode(type);
                }
            }

            return;
        }
        
        if (!gm.equals(nodes.world_gamemode)) {
            
            region = _regions.get(from);

            if (region != null) {
                BCom.msg(p, Messages.region_farewell, region.gamemode.toString().toLowerCase());
            }
            
            if (!_perm.hasPerm(p, "World.Keep")) {
                
                if (nodes.world_gamemode != GameMode.CREATIVE) {
                    BPlayerUtils.toSafeLocation(p);
                }
                
                p.setGameMode(nodes.world_gamemode);
            }
        }
    }
    
    public void gc() {
        release();
    }

    public void release() {
        _config     = null;
        _regions    = null;
        _perm       = null;
    }
}
