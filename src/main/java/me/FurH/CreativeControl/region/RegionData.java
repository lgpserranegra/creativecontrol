package me.FurH.CreativeControl.region;

import org.bukkit.GameMode;
import org.bukkit.Location;

/**
 *
 * @author FurmigaHumana
 * All Rights Reserved unless otherwise explicitly stated.
 */
public class RegionData {

    public int sx;
    public int sy;
    public int sz;
    
    public int ex;
    public int ey;
    public int ez;

    public GameMode gamemode;
    public String   name;

    public RegionData(String name, GameMode gamemode, Location min, Location max) {

        this.sx = min.getBlockX();
        this.sy = min.getBlockY();
        this.sz = min.getBlockZ();
        
        this.ex = max.getBlockX();
        this.ey = max.getBlockY();
        this.ez = max.getBlockZ();

        this.name       = name;
        this.gamemode   = gamemode;
    }

    public boolean contains(Location loc) {
        
        int x = loc.getBlockX();
        int y = loc.getBlockY();
        int z = loc.getBlockZ();

        return x >= sx && x <= ex && y >= sy && y <= ey && z >= sz && z <= ez;
    }
}