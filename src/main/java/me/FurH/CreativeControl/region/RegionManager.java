package me.FurH.CreativeControl.region;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;
import me.FurH.Bukkit.Core.location.BLocationUtils;
import me.FurH.Core.close.Closer;
import me.FurH.Core.database.SQLDb;
import me.FurH.Core.database.SQLWorker;
import me.FurH.Core.debug.Log;
import me.FurH.CreativeControl.EntryPoint;
import me.FurH.CreativeControl.config.MainNodePack;
import me.FurH.CreativeControl.messages.Messages;
import me.FurH.CreativeControl.selection.AreaSelection;
import org.bukkit.GameMode;
import org.bukkit.Location;

/**
 *
 * @author FurmigaHumana
 * All Rights Reserved unless otherwise explicitly stated.
 */
public class RegionManager {

    private final HashMap<Integer, HashMap<Integer, List<Integer>>> index;
    private final HashMap<Integer, RegionData> datas;

    private boolean hasAny = false;

    public RegionManager() {
        index = new HashMap<Integer, HashMap<Integer, List<Integer>>>();
        datas = new HashMap<Integer, RegionData>();
    }

    public RegionData get(Location loc) {
        
        if (!hasAny) {
            return null;
        }

        HashMap<Integer, List<Integer>> inner = index.get(loc.getBlockX() >> 4);

        if (inner != null) {

            List<Integer> ids = inner.get(loc.getBlockZ() >> 4);

            if (ids != null) {

                for (int id : ids) {
                    
                    RegionData region = datas.get(id);
                    
                    if (region.contains(loc)) {
                        return region;
                    }
                }
                
                return null;
            }
        }

        return null;
    }
    
    public void initialize() {
        
        MainNodePack config = EntryPoint.get().getMainNode();
        SQLDb db = EntryPoint.get().getDb();
        
        String sql = "SELECT name, start, end, type FROM `" + config.database_prefix + "regions`;";
        
        Statement st = null;
        ResultSet rs = null;
        
        try {
            
            st = db.statement(false);
            st.execute(sql);
            
            rs = st.getResultSet();
            
            while (rs.next()) {

                hasAny = true;

                String name = rs.getString(1);
                Location start = BLocationUtils.getLocationFromString(rs.getString(2));
                Location end = BLocationUtils.getLocationFromString(rs.getString(3));
                GameMode type = GameMode.valueOf(rs.getString(4).toUpperCase());

                load(name, type, start, end);
            }
            
            if (hasAny) {
                Log.log(Messages.prefix + " Loaded §a" + datas.size() + "§f regions.");
            }

        } catch (SQLException ex) {
            Log.error(ex, "Failed to load gamemode regions");
        } finally {
            Closer.closeQuietly(rs, st);
        }
    }

    public void create(final String name, final GameMode type, AreaSelection selection) {
        
        Location start = selection.getStart();
        Location end = selection.getEnd();
        
        MainNodePack config = EntryPoint.get().getMainNode();
        SQLDb db = EntryPoint.get().getDb();
        
        String sql = "INSERT INTO `"+ config.database_prefix +"regions` (name, start, end, type) VALUES "
                + "("
                    + "'"+name+"',"
                    + "'"+BLocationUtils.getStringFromLocation(start)+"',"
                    + "'"+BLocationUtils.getStringFromLocation(end)+"',"
                    + "'" + type + "'"
                + ");";
        
        Statement st = null;
        
        try {
            
            st = db.statement(true);
            st.execute(sql);
            
            db.commit(false);
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            Closer.closeQuietly(st);
        }
        
        load(name, type, start, end);
    }
    
    public void load(String name, GameMode type, Location min, Location max) {

        HashSet<String> chunks = new HashSet<String>();

        for (int x = min.getBlockX(); x <= max.getBlockX(); x++) {
            for (int z = min.getBlockZ(); z <= max.getBlockZ(); z++) {
                chunks.add((x >> 4) + ":" + (z >> 4));
            }
        }

        RegionData data = new RegionData(name, type, min, max);
        
        int id = nextId();
        datas.put(id, data);

        for (String chunk : chunks) {

            String[] split = chunk.split(":");

            int cx = Integer.parseInt(split[ 0 ]);
            int cz = Integer.parseInt(split[ 1 ]);
            
            if (!index.containsKey(cx)) {
                index.put(cx, new HashMap<Integer, List<Integer>>());
            }

            HashMap<Integer, List<Integer>> inner = index.get(cx);

            if (!inner.containsKey(cz)) {
                inner.put(cz, new ArrayList<Integer>());
            }
            
            List<Integer> local = inner.get(cz);
            
            local.add(id);

            inner.put(cz, local);
            index.put(cx, inner);
        }
    }
    
    private int nextId() {
        
        Random rnd = new Random();
        int id = rnd.nextInt();

        while (datas.containsKey(id)) {
            id = rnd.nextInt();
        }
        
        return id;
    }
    
    public boolean has(String name) {

        for (RegionData data : datas.values()) {
            if (data.name.equalsIgnoreCase(name)) {
                return true;
            }
        }
        
        return false;
    }

    public void remove(final String name) {

        final MainNodePack config = EntryPoint.get().getMainNode();
        final SQLDb db = EntryPoint.get().getDb();
        final SQLWorker worker = db.worker();
        
        worker.enqueue(new Runnable() {
            @Override
            public void run() {
                try {
                    worker.statement(true).execute("DELETE FROM `"+ config.database_prefix +"regions` WHERE `name` = '" + name + "' LIMIT 1;");
                } catch (SQLException ex) {
                    Log.error(ex, "Failed to delete region: " + name);
                }
            }
        });
        
        int key = -1;
        
        Iterator<Entry<Integer, RegionData>> it1 = datas.entrySet().iterator();
        while (it1.hasNext()) {
            Entry<Integer, RegionData> entry = it1.next();
            if (entry.getValue().name.equalsIgnoreCase(name)) {
                key = entry.getKey(); it1.remove();
            }
        }

        for (HashMap<Integer, List<Integer>> inner : index.values()) {
            for (List<Integer> list : inner.values()) {
                Iterator<Integer> it = list.iterator();
                while (it.hasNext()) {
                    if (it.next() == key) {
                        it.remove();
                    }
                }
            }
        }
    }

    public Iterable<RegionData> getAll() {
        return this.datas.values();
    }
}