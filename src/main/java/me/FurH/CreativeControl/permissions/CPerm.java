package me.FurH.CreativeControl.permissions;

import java.util.HashMap;
import java.util.Iterator;
import me.FurH.Core.cache.LimitedSizeMap;
import me.FurH.Core.debug.Log;
import me.FurH.CreativeControl.EntryPoint;
import me.FurH.CreativeControl.config.MainNodePack;
import me.FurH.CreativeControl.messages.Messages;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;

/**
 *
 * @author FurmigaHumana
 * All Rights Reserved unless otherwise explicitly stated.
 */
public class CPerm {

    private final LimitedSizeMap<String, Boolean> cache;

    private MainNodePack _main;
    private Permission vault;

    public CPerm() {
        cache = new LimitedSizeMap<String, Boolean>(5000, new HashMap<String, Boolean>());
    }

    public void setup() {

        PluginManager pm = Bukkit.getPluginManager();
        Plugin plugin = pm.getPlugin("Vault");

        if (plugin != null && plugin.isEnabled()) {

            RegisteredServiceProvider<Permission> permissionProvider = Bukkit.getServicesManager().getRegistration(net.milkbowl.vault.permission.Permission.class);
            if (permissionProvider != null) {
                vault = permissionProvider.getProvider();
                Log.log(Messages.prefix + " Vault hooked as permissions plugin");
            }
        }
    }

    public boolean hasPerm(Player player, String main) {

        main = "CreativeControl." + main;

        if (hasPerm0(player, main, true)) {
            return true;
        }

        String[] splits = main.split("\\.");
        String node = "";

        for (int j1 = 0; j1 < splits.length - 1; j1++) {

            node += (j1 > 0 ? "." : "") + splits[ j1 ];

            boolean has = hasPerm0(player, node, false);

            if (has) {
                cache.put(player.getName() + "_" + player.getWorld().getName() + "_" + main, true); return true;
            }
        }

        return false;
    }
    
    private boolean hasPerm0(Player player, String node, boolean memory) {

        if (_main == null) {
            _main = EntryPoint.get().getMainNode();
        }

        if (player.isOp() && !_main.perm_ophas) {
            return false;
        }

        String key = player.getName() + "_" + player.getWorld().getName() + "_" + node;

        Boolean cached = cache.get(key);

        if (cached != null) {
            return cached;
        }

        boolean ret = player.hasPermission(node);

        if (memory) {
            cache.put(key, ret);
        }

        return ret;
    }
    
    public Permission getVault() {
        return vault;
    }

    public void gc() {
        cache.clear();
        release();
    }

    public void release() {
        _main = null;
    }
    
    public void cleanup(Player p) {
        
        Iterator<String> it = cache.keySet().iterator();
        
        while (it.hasNext()) {
            if (it.next().toLowerCase().startsWith(p.getName().toLowerCase())) {
                it.remove();
            }
        }
    }
}
