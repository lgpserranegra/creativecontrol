package me.FurH.CreativeControl.metrics;

import java.io.IOException;
import me.FurH.Core.database.SQLDb;
import me.FurH.CreativeControl.CreativeControl;
import me.FurH.CreativeControl.EntryPoint;
import me.FurH.CreativeControl.config.MainNodePack;
import me.FurH.CreativeControl.config.world.WorldConfig;
import me.FurH.CreativeControl.config.world.WorldNodePack;
import me.FurH.CreativeControl.metrics.Metrics.Graph;
import me.FurH.CreativeControl.metrics.Metrics.Plotter;
import me.FurH.CreativeControl.region.RegionData;
import me.FurH.CreativeControl.region.RegionManager;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.World;

/**
 *
 * @author FurmigaHumana All Rights Reserved unless otherwise explicitly stated.
 */
public class MetricsManager {

    public void startMetrics() {

        try {

            CreativeControl cc = EntryPoint.get();

            Metrics metrics = new Metrics(EntryPoint.getPlugin());

            Graph dbType = metrics.createGraph("Database Type");
            SQLDb db = cc.getDb();

            dbType.addPlotter(new Plotter(db.getEngineType().toString()) {
                @Override
                public int getValue() {
                    return 1;
                }
            });

            RegionManager regions = cc.getRegionManager();

            int creative = 0;
            int survival = 0;

            for (RegionData CR : regions.getAll()) {
                if (CR.gamemode == GameMode.CREATIVE) {
                    creative++;
                } else {
                    survival++;
                }
            }

            final int total = creative + survival;

            Graph reg = metrics.createGraph("Regions");
            reg.addPlotter(new Plotter("Regions") {
                @Override
                public int getValue() {
                    return total;
                }
            });

            final int creative0 = creative;

            Graph reg1 = metrics.createGraph("Regions Type");
            reg1.addPlotter(new Plotter("Creative") {
                @Override
                public int getValue() {
                    return creative0;
                }
            });

            final int survival0 = survival;

            reg1.addPlotter(new Plotter("Survival") {

                @Override
                public int getValue() {
                    return survival0;
                }

            });

            /*
             extra.addPlotter(new CreativeMetrics.Plotter("Misc Protection") {
             @Override
             public int getValue() {
             return useMisc;
             }
             });*/
            WorldConfig config = cc.getWorldConfig();

            int OwnBlock = 0;
            int NoDrop = 0;

            for (World world : Bukkit.getServer().getWorlds()) {

                WorldNodePack nodes = config.get(world.getName());

                //if (nodes.block_ownblock) {
                //    OwnBlock++;
                //} else
                if (nodes.block_nodrop) {
                    NoDrop++;
                }
            }

            final int OwnBlock0 = OwnBlock;

            Graph ptype = metrics.createGraph("Protection Type");
            ptype.addPlotter(new Plotter("OwnBlocks") {
                @Override
                public int getValue() {
                    return OwnBlock0;
                }
            });

            final int NoDrop0 = NoDrop;

            ptype.addPlotter(new Plotter("NoDrop") {
                @Override
                public int getValue() {
                    return NoDrop0;
                }
            });

            metrics.start();

        } catch (IOException e) {
        }
    }
}
