package me.FurH.CreativeControl.selection;

import org.bukkit.Location;

/**
 *
 * @author FurmigaHumana
 * All Rights Reserved unless otherwise explicitly stated.
 */
public class AreaSelection {

    private final Location start, end;

    public AreaSelection(Location start, Location end) throws Exception {

        if (start.getWorld() != end.getWorld()) {
            throw new Exception("The start and end point must be in the same world!");
        }
        
        int sx = start.getBlockX();
        int ex = end.getBlockX();
        
        if (sx > ex) {
            int i = sx;
            sx = ex;
            ex = i;
        }

        int sy = start.getBlockY();
        int ey = end.getBlockY();
        
        if (sy > ey) {
            int i = sy;
            sy = ey;
            ey = i;
        }

        int sz = start.getBlockZ();
        int ez = end.getBlockZ();
        
        if (sz > ez) {
            int i = sz;
            sz = ez;
            ez = i;
        }

        this.start = new Location(start.getWorld(), sx, sy, sz);
        this.end = new Location(start.getWorld(), ex, ey, ez);
    }

    public void expandUpUp(int up) {
        this.end.add(0, up, 0);
    }
    
    public void contratUpUp(int contrat) {
        this.end.subtract(0, contrat, 0);
    } 
    
    public void expandUpDown(int up) {
        this.start.add(0, up, 0);
    }
    
    public void contratUpDown(int contrat) {
        this.start.subtract(0, contrat, 0);
    }
    
    public void expandVert() {
        this.start.setY(0);
        this.end.setY(255);
    }
    
    public Location getStart() {
        return start;
    }
    
    public Location getEnd() {
        return end;
    }
    
    public int getArea() {

        Location min = start;
        Location max = end;

        return ((max.getBlockX() - min.getBlockX() + 1) *
                (max.getBlockY() - min.getBlockY() + 1) *
                (max.getBlockZ() - min.getBlockZ() + 1));
    }
}
