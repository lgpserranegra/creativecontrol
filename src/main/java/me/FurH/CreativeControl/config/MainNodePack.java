package me.FurH.CreativeControl.config;

import java.io.File;
import me.FurH.Bukkit.Core.configuration.BConfigLoader;
import me.FurH.Core.database.SQLDb.CoreType;
import me.FurH.Core.debug.Log;
import me.FurH.Core.object.ObjectUtils;
import me.FurH.CreativeControl.EntryPoint;
import me.FurH.CreativeControl.messages.Messages;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author FurmigaHumana
 * All Rights Reserved unless otherwise explicitly stated.
 */
public class MainNodePack {

    public CoreType        database_type;
    public String          database_host;
    public int             database_port;
    public String          database_user;
    public String          database_pass;
    public String          database_table;
    public String          database_prefix;
    
    public int             threads;
    public double          thread_speed;
    public boolean         keepalive;
    public int             timeout;

    public File            sqlite_file;
    
    public boolean         perm_enabled;
    public String          perm_creative;
    public boolean         perm_keep;
    public boolean         perm_ophas;

    public int             cache_capacity;

    public boolean         config_single;
    public boolean         config_conflict;
    public boolean         config_friend;

    public boolean         updater_enabled;

    public Material        selection_tool;
    
    public boolean         data_inventory;
    public boolean         data_status;
    public boolean         data_teleport;
    public boolean         data_survival;
    
    public ItemStack       armor_helmet;
    public ItemStack       armor_chest;
    public ItemStack       armor_leggs;
    public ItemStack       armor_boots;
    
    public boolean         com_quiet;
    public boolean         com_debugcons;
    public boolean         com_debugstack;

    public void load() {

        long start = System.currentTimeMillis();
        
        JavaPlugin plugin = EntryPoint.getPlugin();
        
        File input = new File(plugin.getDataFolder(), "settings.yml");
        BConfigLoader loader = new BConfigLoader(plugin, "settings.yml", input);
        
        Log.log(Messages.prefix + " Loading file: " + input.getName() + "...");
        
        try {
            loader.load();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        database_type     = ObjectUtils.toString(loader.get("Database.Type", "SQLite")).equalsIgnoreCase("SQLite") ? CoreType.SQLite : CoreType.MySQL;
        database_host     = ObjectUtils.toString(loader.get("Database.host", "localhost"));
        database_port     = ObjectUtils.toInteger(loader.get("Database.port", 3306));
        database_user     = ObjectUtils.toString(loader.get("Database.user", "root"));
        database_pass     = ObjectUtils.toString(loader.get("Database.pass", ""));
        database_table    = ObjectUtils.toString(loader.get("Database.table", "minecraft"));
        database_prefix   = ObjectUtils.toString(loader.get("Database.prefix", "crcr_"));

        threads           = ObjectUtils.toInteger(loader._get("Database._threads"));
        thread_speed      = ObjectUtils.toDouble(loader._get("Database._thread_speed"));
        keepalive         = ObjectUtils.toBoolean(loader._get("Database._keepalive"));
        timeout           = ObjectUtils.toInteger(loader._get("Database._timeout"));

        sqlite_file       = new File(ObjectUtils.toString(loader._get("Database._sqlfile")).replace("$pluginfolder$", plugin.getDataFolder().getAbsolutePath()));
        
        perm_enabled      = ObjectUtils.toBoolean(loader.get("Permissions.ChangeGroups", false));
        perm_keep         = ObjectUtils.toBoolean(loader.get("Permissions.KeepCurrentGroup", true));
        perm_creative     = ObjectUtils.toString(loader.get("Permissions.CreativeGroup", "MembersCreative"));
        perm_ophas        = ObjectUtils.toBoolean(loader.get("Permissions.OpHasPerm", false));

        cache_capacity    = ObjectUtils.toInteger(loader._get("_Cache._MaxCapacity"));

        config_single     = ObjectUtils.toBoolean(loader.get("Configurations.Single", false));
        config_conflict   = ObjectUtils.toBoolean(loader._get("Configurations._Conflict"));
        config_friend     = ObjectUtils.toBoolean(loader.get("Configurations.FriendSystem", false));

        updater_enabled   = ObjectUtils.toBoolean(loader.get("Updater.Enabled", false));

        selection_tool    = Material.getMaterial(ObjectUtils.toString(loader._get("Selection.Tool")).toUpperCase());

        data_inventory    = ObjectUtils.toBoolean(loader.get("PlayerData.Inventory", true));
        data_status       = ObjectUtils.toBoolean(loader.get("PlayerData.Status", true));
        data_teleport     = ObjectUtils.toBoolean(loader.get("PlayerData.Teleport", false));
        data_survival     = ObjectUtils.toBoolean(loader.get("PlayerData.SetSurvival", false));

        armor_helmet      = (ItemStack) loader.get("CreativeArmor.Helmet", new ItemStack(Material.AIR));
        armor_chest       = (ItemStack) loader.get("CreativeArmor.Chestplate", new ItemStack(Material.AIR));
        armor_leggs       = (ItemStack) loader.get("CreativeArmor.Leggings", new ItemStack(Material.AIR));
        armor_boots       = (ItemStack) loader.get("CreativeArmor.Boots", new ItemStack(Material.AIR));

        com_quiet         = ObjectUtils.toBoolean(loader._get("_Communicator._Quiet"));
        com_debugcons     = ObjectUtils.toBoolean(loader._get("_Debug._Console"));
        com_debugstack    = ObjectUtils.toBoolean(loader._get("_Debug._Stack"));

        try {
            loader.save(input);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        Log.log(Messages.prefix + " File " + input.getName() + " loaded in " + (System.currentTimeMillis() - start) + " ms");
    }
}