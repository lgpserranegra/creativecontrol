package me.FurH.CreativeControl.config.world;

import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;
import me.FurH.Core.debug.Log;
import me.FurH.Core.reference.ExpirableReference;
import me.FurH.CreativeControl.messages.Messages;
import org.bukkit.World;

/**
 *
 * @author FurmigaHumana
 * All Rights Reserved unless otherwise explicitly stated.
 */
public class WorldConfig {
    
    private final HashMap<String, ExpirableReference<WorldNodePack>> references;
    
    public WorldConfig() {
        this.references = new HashMap<String, ExpirableReference<WorldNodePack>>();
    }

    public void unload() {
        
        synchronized (references) {
            Iterator<ExpirableReference<WorldNodePack>> it = references.values().iterator();
            
            while (it.hasNext()) {
                it.next().clear(); it.remove();
            }
            
            references.clear();
        }
    }
    
    public WorldNodePack get(final String world) {
        
        WorldNodePack ret = null;
        
        if (references.containsKey(world)) {
            ret = references.get(world).get();
        }
        
        if (ret == null) {

            ret = new WorldNodePack();
            ret.load(world);

            references.put(world, new ExpirableReference<WorldNodePack>(ret, 5, TimeUnit.MINUTES, new Runnable() {
                @Override
                public void run() {
                    Log.log(Messages.prefix + " File " + world + ".yml has expired.");
                }
            }));
        }

        return ret;
    }

    public void unload(World world) {

        ExpirableReference<WorldNodePack> nodes = references.get(world.getName());

        if (nodes != null) {
            nodes.clear();
        }

        references.remove(world.getName());
    }
}
