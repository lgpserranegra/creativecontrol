package me.FurH.CreativeControl.config.world;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import me.FurH.Bukkit.Core.configuration.BConfigLoader;
import me.FurH.Core.debug.Log;
import me.FurH.Core.number.NumberUtils;
import me.FurH.Core.object.ObjectUtils;
import me.FurH.CreativeControl.EntryPoint;
import me.FurH.CreativeControl.messages.Messages;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author FurmigaHumana
 * All Rights Reserved unless otherwise explicitly stated.
 */
public class WorldNodePack {

    public GameMode world_gamemode;
    public boolean  world_exclude;
    public boolean  world_changegm;

    public HashSet<String> black_cmds;
    public HashSet<String> black_s_cmds;

    public HashSet<Object> black_place;
    public HashSet<Object> black_break;
    public HashSet<Object> black_use;
    public HashSet<Object> black_interact;
    public HashSet<Object> black_inventory;
    public HashSet<String> black_sign;

    public boolean misc_tnt;
    public boolean misc_ice;
    public boolean misc_liquid;
    public boolean misc_fire;

    public boolean block_water;
    public boolean block_worledit;
    public boolean block_ownblock;
    public boolean block_nodrop;
    public boolean block_explosion;
    public boolean block_creative;
    public boolean block_pistons;
    public boolean block_physics;
    public boolean block_against;
    public boolean block_attach;
    public boolean block_invert;
    public HashSet<Object> block_exclude;
    public int block_minutelimit;

    public boolean prevent_drop;
    public boolean prevent_pickup;
    public boolean prevent_pvp;
    public boolean prevent_mobs;
    public boolean prevent_eggs;
    public boolean prevent_target;
    public boolean prevent_mobsdrop;
    public boolean prevent_irongolem;
    public boolean prevent_snowgolem;
    public boolean prevent_wither;
    public boolean prevent_drops;
    public boolean prevent_enchant;
    public boolean prevent_mcstore;
    public boolean prevent_bedrock;
    public boolean prevent_invinteract;
    public boolean prevent_bonemeal;
    public boolean prevent_villager;
    public boolean prevent_potion;
    public boolean prevent_frame;
    public boolean prevent_vehicle;
    public int prevent_limitvechile;
    public int prevent_stacklimit;
    public boolean prevent_open;
    public boolean prevent_fly;
    public boolean prevent_creative;

    public void load(String world) {
        
        long start = System.currentTimeMillis();
        
        JavaPlugin plugin = EntryPoint.getPlugin();

        File input = new File(plugin.getDataFolder(), "worlds" + File.separator + world + ".yml");
        BConfigLoader loader = new BConfigLoader(plugin, "world.yml", input);

        Log.log(Messages.prefix + " Loading file: " + input.getName() + "...");

        try {
            loader.load();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        world_gamemode = GameMode.valueOf(ObjectUtils.toString(loader.get("World.GameMode", "SURVIVAL")));

        world_exclude         = ObjectUtils.toBoolean(loader.get("World.Exclude", false));
        world_changegm        = ObjectUtils.toBoolean(loader.get("World.ChangeGameMode", false));

        black_cmds            = getSet(loader, "BlackList.Commands");
        black_s_cmds          = getSet(loader, "BlackList.SurvivalCommands");

        black_place           = getMaterialSet(loader, "BlackList.BlockPlace", "SurvivalCommands");
        black_break           = getMaterialSet(loader, "BlackList.BlockBreak", "BlockPlace");
        black_use             = getMaterialSet(loader, "BlackList.ItemUse", "BlockBreak");
        black_interact        = getMaterialSet(loader, "BlackList.ItemInteract", "ItemUse");
        black_inventory       = getMaterialSet(loader, "BlackList.Inventory", "ItemInteract");
        
        black_sign            = getSet(loader, "BlackList.SignText");
    
        misc_tnt              = ObjectUtils.toBoolean(loader.get("MiscProtection.NoTNTExplosion", false));
        misc_ice              = ObjectUtils.toBoolean(loader.get("MiscProtection.IceMelt", false));
        misc_liquid           = ObjectUtils.toBoolean(loader.get("MiscProtection.LiquidControl", false));
        misc_fire             = ObjectUtils.toBoolean(loader.get("MiscProtection.Fire", false));
    
        block_water           = ObjectUtils.toBoolean(loader.get("BlockProtection.WaterFlow", false));
        block_worledit        = ObjectUtils.toBoolean(loader.get("BlockProtection.WorldEdit", false));
        block_ownblock        = ObjectUtils.toBoolean(loader.get("BlockProtection.OwnBlocks", false));
        block_nodrop          = ObjectUtils.toBoolean(loader.get("BlockProtection.NoDrop", true));
        block_explosion       = ObjectUtils.toBoolean(loader.get("BlockProtection.Explosions", false));
        block_creative        = ObjectUtils.toBoolean(loader.get("BlockProtection.CreativeOnly", false));
        block_pistons         = ObjectUtils.toBoolean(loader.get("BlockProtection.Pistons", false));
        block_physics         = ObjectUtils.toBoolean(loader.get("BlockProtection.Physics", false));
        block_against         = ObjectUtils.toBoolean(loader.get("BlockProtection.BlockAgainst", false));
        block_attach          = ObjectUtils.toBoolean(loader.get("BlockProtection.CheckAttached", false));
        block_invert          = ObjectUtils.toBoolean(loader.get("BlockProtection.inverted", false));
        block_exclude         = getMaterialSet(loader, "BlockProtection.exclude", "inverted");
        block_minutelimit     = ObjectUtils.toInteger(loader.get("BlockProtection.BlockPerMinute", -1));
    
        prevent_drop          = ObjectUtils.toBoolean(loader.get("Preventions.ItemDrop", false));
        prevent_pickup        = ObjectUtils.toBoolean(loader.get("Preventions.ItemPickup", false));
        prevent_pvp           = ObjectUtils.toBoolean(loader.get("Preventions.PvP", false));
        prevent_mobs          = ObjectUtils.toBoolean(loader.get("Preventions.Mobs", false));
        prevent_eggs          = ObjectUtils.toBoolean(loader.get("Preventions.Eggs", false));
        prevent_target        = ObjectUtils.toBoolean(loader.get("Preventions.Target", false));
        prevent_mobsdrop      = ObjectUtils.toBoolean(loader.get("Preventions.MobsDrop", false));
        prevent_irongolem     = ObjectUtils.toBoolean(loader.get("Preventions.IronGolem", false));
        prevent_snowgolem     = ObjectUtils.toBoolean(loader.get("Preventions.SnowGolem", false));
        prevent_wither        = ObjectUtils.toBoolean(loader.get("Preventions.Wither", false));
        prevent_drops         = ObjectUtils.toBoolean(loader.get("Preventions.ClearDrops", false));
        prevent_enchant       = ObjectUtils.toBoolean(loader.get("Preventions.Enchantments", false));
        prevent_mcstore       = ObjectUtils.toBoolean(loader.get("Preventions.MineCartStorage", false));
        prevent_bedrock       = ObjectUtils.toBoolean(loader.get("Preventions.BreakBedRock", false));
        prevent_invinteract   = ObjectUtils.toBoolean(loader.get("Preventions.InvInteract", false));
        prevent_bonemeal      = ObjectUtils.toBoolean(loader.get("Preventions.Bonemeal", false));
        prevent_villager      = ObjectUtils.toBoolean(loader.get("Preventions.InteractVillagers", false));
        prevent_potion        = ObjectUtils.toBoolean(loader.get("Preventions.PotionSplash", false));
        prevent_frame         = ObjectUtils.toBoolean(loader.get("Preventions.ItemFrame", false));
        prevent_vehicle       = ObjectUtils.toBoolean(loader.get("Preventions.VehicleDrop", false));
        prevent_limitvechile  = ObjectUtils.toInteger(loader.get("Preventions.VehicleLimit", 1));
        prevent_stacklimit    = ObjectUtils.toInteger(loader.get("Preventions.StackLimit", -1));
        prevent_open          = ObjectUtils.toBoolean(loader.get("Preventions.InventoryOpen", false));
        prevent_fly           = ObjectUtils.toBoolean(loader.get("Preventions.RemoveFlyOnPvP", false));
        prevent_creative      = ObjectUtils.toBoolean(loader.get("Preventions.NoCreativeOnPvP", false));

        try {
            loader.save(input);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        Log.log(Messages.prefix + " File " + input.getName() + " loaded in " + (System.currentTimeMillis() - start) + " ms");
    }

    private HashSet<Object> getMaterialSet(BConfigLoader loader, String key, String after) {

        List<Object> temp = new ArrayList<Object>();
        boolean shouldsave = false;
        
        List<Object> list = (List<Object>) loader.get(key, new ArrayList<Object>());
        for (Object obj : list) {

            if (obj instanceof Number) {

                int id = ((Number)obj).intValue();
                Material mat = Material.getMaterial(id);

                shouldsave = true;

                if (mat != null) {
                    temp.add(mat);
                } else {
                    Log.log("Invalid material id: " + id);
                }
            }
            
            if (obj instanceof String) {
                
                String str = (String) obj;
                                
                if (str.contains(":")) {
                    
                    String[] splits = str.split(":");
                                        
                    if (NumberUtils.isInteger(splits[ 0 ])) {
                        
                        shouldsave = true;
                        
                        int id = NumberUtils.toInteger(splits[ 0 ]);
                        Material mat = Material.getMaterial(id);
                                                
                        if (mat != null) {
                            temp.add(mat + ":" + splits[ 1 ]);
                        } else {
                            Log.log("Invalid id:data id: " + id);
                        }
                        
                    } else {
                        
                        Material mat = Material.getMaterial(splits[ 0 ]);
                        
                        if (mat == null) {
                            
                            Log.log("Invalid material:data name: " + str + " ( " + splits[ 0 ] + " )" );
                        
                        } else {
                            temp.add(mat + ":" + splits[ 1 ]);
                        }
                    }
                    
                } else {
                    
                    Material mat = Material.getMaterial(str);
                    
                    if (mat != null) {
                        temp.add(mat);
                    } else {
                        Log.log("Invalid material name: " + str);
                    }
                }
            }
        }
        
        if (shouldsave) {
            
            List<String> mats = new ArrayList<String>();
            
            for (Object obj : temp) {
                mats.add(obj.toString());
            }
            
            loader.set(key, mats, after);
        }
        
        return new HashSet<Object>(temp);
    }

    private HashSet<String> getSet(BConfigLoader loader, String key) {
        return new HashSet<String>((Collection<? extends String>) loader.get(key, new ArrayList<String>()));
    }
}