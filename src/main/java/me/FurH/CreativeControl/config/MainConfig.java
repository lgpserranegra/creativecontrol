package me.FurH.CreativeControl.config;

import java.util.concurrent.TimeUnit;
import me.FurH.Core.debug.Log;
import me.FurH.Core.reference.ExpirableReference;
import me.FurH.CreativeControl.messages.Messages;

/**
 *
 * @author FurmigaHumana
 * All Rights Reserved unless otherwise explicitly stated.
 */
public class MainConfig {
    
    private ExpirableReference<MainNodePack> reference;

    public void load() {

        MainNodePack nodes = new MainNodePack();

        /* ** initialize nodes ** */
        nodes.load();

        /* ** keep it in memory ** */
        this.reference = new ExpirableReference<MainNodePack>(nodes, 5, TimeUnit.MINUTES, new Runnable() {
            @Override
            public void run() {
                Log.log(Messages.prefix + " File settings.yml has expired.");
            }
        });
    }

    public void unload() {
        
        if (reference == null) {
            return;
        }
        
        reference.clear();
        reference = null;
    }

    public MainNodePack get() {

        if (reference == null || reference.get() == null) {
            this.load();
        }

        return reference.get();
    }
}